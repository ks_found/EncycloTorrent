package org.encyclosphere.encyclotorrent.api.zwi;

import me.tongfei.progressbar.ProgressBar;
import me.tongfei.progressbar.ProgressBarBuilder;
import me.tongfei.progressbar.ProgressBarStyle;
import net.lingala.zip4j.exception.ZipException;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.encyclosphere.encyclotorrent.api.index.IndexEntry;
import org.encyclosphere.encyclotorrent.api.index.IndexFile;
import org.encyclosphere.encyclotorrent.api.misc.Language;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.*;

/**
 * Represents a database of ZWI files on disk.
 */
public class ZWIDatabase {

    private final Path root;

    private long articleCount, size;

    private static IndexSearcher searcher;

    private static long lastIndexLoadTime;



    /**
     * Creates a {@link ZWIDatabase}.
     * @param root The {@link Path} to the database
     * @throws IllegalArgumentException If the given database {@code Path} isn't a directory
     * @throws IOException If the database root doesn't exist and couldn't be created
     */
    public ZWIDatabase(Path root) throws IOException {
        if(Files.exists(root) && !Files.isDirectory(root))
            throw new IllegalArgumentException("Database root is not a directory");
        Files.createDirectories(root);
        this.root = root;
    }



    /**
     * @return The root of this {@link ZWIDatabase}
     */
    public Path getRoot() {
        return root;
    }

    /**
     * Adds a ZWI file to this {@link ZWIDatabase}, overwriting any existing files with the same ID.<br>
     * If you're adding files to the database in bulk,
     * call {@link ZWIDatabase#addFile(Path, boolean, boolean)} instead (with the last argument set to {@code false}),
     * then call {@link ZWIDatabase#buildAllIndexes()} when you're done.
     * @param file The {@link Path} to the file to add
     * @return The file in the database, as a {@link ZWIFile}
     * @throws IOException If an error occurs when moving/copying the ZWI file, creating the required directories, or writing to index files
     */
    public ZWIFile addFile(Path file) throws IOException {
        return addFile(file, false, true);
    }

    /**
     * Adds a ZWI file to this {@link ZWIDatabase}, overwriting any existing files with the same ID.
     *
     * @param file The file to add
     * @param move Whether to move the file.<br>
     *             If this is {@code true}, the file will be moved from its current location to a new location in the database.<br>
     *             If it's {@code false}, the file will be copied to the database.
     * @param addToIndexes Whether to add the file to the database's indexes.
     *                     If you're adding files to the database in bulk, set this to {@code false}, and call {@link ZWIDatabase#buildAllIndexes()} when you're done adding files.
     *                     If not, set this to {@code true}.
     * @return The file in the database, as a {@link ZWIFile}
     * @throws IOException If an error occurs when moving/copying the ZWI file, creating the required directories, or writing to index files
     */
    public ZWIFile addFile(Path file, boolean move, boolean addToIndexes) throws IOException {

        // Move the file to the correct directory
        ZWIFile originalZWIFile = new ZWIFile(file);
        Path targetPath = Utilities.getPathInDatabase(this, originalZWIFile.getMetadata());

        // Create required directories
        Files.createDirectories(targetPath.getParent());

        // Move or copy the ZWI file to the database
        if(move) Files.move(file, targetPath, StandardCopyOption.REPLACE_EXISTING);
        else Files.copy(file, targetPath, StandardCopyOption.REPLACE_EXISTING);

        // Get the ZWI file in the new location as a ZWIFile object
        ZWIFile zwiFile = new ZWIFile(this, targetPath);

        // Add the ZWI file to the indexes
        if(addToIndexes) addToIndexes(zwiFile);

        return zwiFile;
    }

    /**
     * Adds a ZWI file to this {@link ZWIDatabase}, overwriting any existing files with the same ID.
     * If you're adding files to the database in bulk,
     * call {@link ZWIDatabase#addFile(ZWIMetadata, byte[], boolean)} instead (with the last argument set to {@code false}),
     * then call {@link ZWIDatabase#buildAllIndexes()} when you're done.
     * @param contents The contents of a ZWI file, as a byte array
     * @return The file in the database, as a {@link ZWIFile}
     * @throws IOException If an error occurs when creating the ZWI file, creating the required directories, or writing to index files
     */
    public ZWIFile addFile(byte[] contents) throws IOException {
        ZWIMetadata metadata = ZWIMetadata.fromZWIBytes(contents);
        if(metadata == null) throw new IOException("Invalid ZWI file: Missing metadata.json");
        return addFile(metadata, contents, true);
    }

    /**
     * Adds a ZWI file to this {@link ZWIDatabase}, overwriting any existing files with the same ID.
     * @param metadata The {@link ZWIMetadata} to use. This method uses the language, publisher, and ID.
     * @param contents The contents of the file, as a byte array
     * @param addToIndexes Whether to add the file to the database's indexes.
     *                     If you're adding files to the database in bulk, set this to {@code false}, and call {@link ZWIDatabase#buildAllIndexes()} when you're done adding files.
     *                     If not, set this to {@code true}.
     * @return The file in the database as a {@link ZWIFile}, or {@code null} if it was invalid
     * @throws IOException If an error occurs when creating the ZWI file, creating the required directories, or writing to index files
     */
    public ZWIFile addFile(ZWIMetadata metadata, byte[] contents, boolean addToIndexes) throws IOException {

        // Get the path
        Path targetPath = Utilities.getPathInDatabase(this, metadata);

        // Create required directories
        Files.createDirectories(targetPath.getParent());

        // Add the file to the database
        Files.write(targetPath, contents);

        // Get the file as a ZWIFile object
        ZWIFile zwiFile = new ZWIFile(this, targetPath);

        // Add the ZWI file to the indexes
        if(addToIndexes) addToIndexes(zwiFile);

        return zwiFile;
    }

    /**
     * Permanently deletes a {@link ZWIFile} from this {@link ZWIDatabase}.
     * If you're deleting files from the database in bulk,
     * call {@link ZWIDatabase#deleteFile(ZWIFile, boolean)} instead (with the last argument set to {@code false}),
     * then call {@link ZWIDatabase#buildAllIndexes()} when you're done.
     * @param file The file to delete
     * @see ZWIDatabase#trashFile(ZWIFile)
     */
    public void deleteFile(ZWIFile file) throws IOException {
        deleteFile(file, true);
    }

    /**
     * Permanently deletes a {@link ZWIFile} from this {@link ZWIDatabase}.
     * @param file The file to delete
     * @param removeFromIndexes Whether to remove the file from the database's indexes.
     *                          If you're deleting files from the database in bulk, set this to {@code false}, and call {@link ZWIDatabase#buildAllIndexes()} when you're done deleting files.
     *                          Otherwise, use {@link ZWIDatabase#deleteFile(ZWIFile)}.
     */
    public void deleteFile(ZWIFile file, boolean removeFromIndexes) throws IOException {
        Files.delete(file.getLocation());
        if(removeFromIndexes) removeFromIndexes(file);
    }

    /**
     * Moves a {@link ZWIFile} to this {@link ZWIDatabase}'s trash.
     * If you're deleting files from the database in bulk,
     * call {@link ZWIDatabase#trashFile(ZWIFile, boolean)} instead (with the last argument set to {@code false}),
     * then call {@link ZWIDatabase#buildAllIndexes()} when you're done.
     * @param file The file to move to the trash
     * @see ZWIDatabase#deleteFile(ZWIFile)
     */
    public void trashFile(ZWIFile file) throws IOException {
        trashFile(file, true);
    }

    /**
     * Moves a {@link ZWIFile} to this {@link ZWIDatabase}'s trash.
     * @param file The file to move to the trash
     * @param removeFromIndexes Whether to remove the file from the database's indexes.
     *                          If you're deleting files from the database in bulk, set this to {@code false}, and call {@link ZWIDatabase#buildAllIndexes()} when you're done deleting files.
     *                          Otherwise, use {@link ZWIDatabase#trashFile(ZWIFile)}.
     */
    public void trashFile(ZWIFile file, boolean removeFromIndexes) throws IOException {
        ZWIMetadata metadata = file.getMetadata();
        Path targetPath = root
                .resolve(metadata.getLanguageCode())
                .resolve("trash")
                .resolve(file.getPartialPath())
                .resolve(metadata.getID() + "@" + Utilities.getSHA1Hash(Files.readAllBytes(file.getLocation())) + ".zwi");
        Files.createDirectories(targetPath.getParent());
        Files.move(file.getLocation(), targetPath, StandardCopyOption.REPLACE_EXISTING);
        if(removeFromIndexes) removeFromIndexes(file);
    }

    /**
     * Empties this {@link ZWIDatabase}'s trash.
     */
    public void emptyTrash() throws IOException {
        for(Path path : getLanguages()) {
            Path trash = path.resolve("trash");
            Files.walkFileTree(trash, new SimpleFileVisitor<>() {

                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    if(!file.toString().endsWith(".zwi")) return FileVisitResult.CONTINUE;
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

            });
        }
    }

    /**
     * Adds a {@link ZWIFile} to this {@link ZWIDatabase}'s indexes.
     * @param zwiFile The {@link ZWIFile} to add
     */
    public void addToIndexes(ZWIFile zwiFile) throws IOException {
        IndexFile indexFile = zwiFile.getIndexFile();
        indexFile.addEntry(zwiFile, true);
        indexFile.write();

        IndexWriter writer = new IndexWriter(FSDirectory.open(root.resolve("lucene").toAbsolutePath()), getIndexWriterConfig());
        writer.deleteDocuments(new Term("id", zwiFile.getID())); // Remove old documents with the same ID
        writer.addDocument(zwiFile.getLuceneDocument()); // Add the new document
        writer.close();
    }

    /**
     * Removes a {@link ZWIFile} from this {@link ZWIDatabase}'s indexes.
     * @param zwiFile The {@link ZWIFile} to remove
     */
    public void removeFromIndexes(ZWIFile zwiFile) throws IOException {
        IndexFile indexFile = zwiFile.getIndexFile();
        indexFile.removeEntries(zwiFile);
        indexFile.write();

        IndexWriter writer = new IndexWriter(FSDirectory.open(root.resolve("lucene").toAbsolutePath()), getIndexWriterConfig());
        writer.deleteDocuments(new Term("id", zwiFile.getID()));
        writer.close();
    }

    /**
     * Gets the {@link ZWIFile} in this {@link ZWIDatabase} with the given ID.
     * @param id The ID
     * @return The corresponding {@code ZWIFile}, or {@code null} if it couldn't be found
     */
    public ZWIFile getZWIFile(String id) throws IOException {
        Path path = getZWIPath(id);
        if(path != null) return new ZWIFile(this, path);
        else return null;
    }

    /**
     * @param id An article ID
     * @return The {@link Path} to the ZWI file in this {@link ZWIDatabase} with the given ID, or {@code null} if it couldn't be found
     */
    public Path getZWIPath(String id) throws IOException {
        id = processID(id);
        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(!Files.isDirectory(publisherPath)) continue;
                Path zwiPath = publisherPath.resolve(getPartialPath(id)).resolve(id + ".zwi");
                if(Files.exists(zwiPath)) return zwiPath;
            }
        }
        return null;
    }

    /**
     * @param metadata A {@link ZWIMetadata} object
     * @return The {@link IndexFile} in this database with the same language and publisher as the given {@link ZWIMetadata},
     *         or {@code null} if such a file couldn't be found
     */
    public IndexFile getIndexFile(ZWIMetadata metadata) throws IOException {
        Path indexPath = root
                .resolve(metadata.getLanguageCode())
                .resolve("zwi")
                .resolve(metadata.getPublisher())
                .resolve("index.csv.gz");

        if(Files.notExists(indexPath)) buildIndexFiles();

        IndexFile indexFile = new IndexFile(indexPath);
        if(indexFile.hasEntry(metadata.getID())) return indexFile;
        return null;
    }

    /**
     * Syncs this {@link ZWIDatabase} with an aggregator's database.
     * This method relies on index files, so you may need to call {@link ZWIDatabase#buildIndexFiles()} first.
     * @param aggregatorURL The URL of the aggregator to sync the database with
     * @param showProgressBar Whether to show an ASCII progress bar
     * @return {@code true} if the sync was successful, {@code false} otherwise
     */
    public boolean syncWith(String aggregatorURL, boolean showProgressBar) throws IOException {

        // Get a list of indexes
        HttpResponse response = makeRequest(new HttpGet(aggregatorURL + "/indexes"));
        if(response.getStatusLine().getStatusCode() != 200) return false;
        String[] indexList = new String(response.getEntity().getContent().readAllBytes()).split("\n");

        boolean indexNeedsUpdate = false;

        // Iterate over the list of indexes
        for(String indexPath : indexList) {

            // Get the language and publisher of each index
            String[] pathSplit = indexPath.split("/");
            String language = pathSplit[1];
            String publisher = pathSplit[3];

            // Download the index
            HttpResponse indexGetResponse = makeRequest(new HttpGet(aggregatorURL + "/" + indexPath));
            if(response.getStatusLine().getStatusCode() != 200) return false;
            byte[] indexBytes = indexGetResponse.getEntity().getContent().readAllBytes();

            // Compare the index file downloaded from the aggregator to the local one
            List<IndexEntry> filesToDownload, filesToDelete = new ArrayList<>();
            IndexFile remoteIndexFile = new IndexFile(new String(gunzip(indexBytes)));

            IndexFile localIndexFile;
            Path localIndexPath = root.resolve(language).resolve("zwi").resolve(publisher).resolve("index.csv.gz");
            if(Files.exists(localIndexPath)) {
                localIndexFile = new IndexFile(localIndexPath);

                List<IndexEntry> remoteEntries = remoteIndexFile.getEntries();
                List<IndexEntry> localEntries = localIndexFile.getEntries();
                filesToDownload = new ArrayList<>(remoteEntries);
                filesToDelete = new ArrayList<>(localEntries);

                ProgressBar indexCompareBar = null;
                if(showProgressBar) {
                    indexCompareBar = new ProgressBarBuilder()
                            .setTaskName("Comparing index files: " + language + "/" + publisher)
                            .setInitialMax((long) localEntries.size() * remoteEntries.size())
                            .setMaxRenderedLength(130)
                            .setUpdateIntervalMillis(200)
                            .setStyle(ProgressBarStyle.ASCII)
                            .build();
                }

                // If the local database has a file that the remote database doesn't,
                // move the file to the local database's trash.
                // If the local and remote databases both have a file,
                // but the remote database has a newer version, download the newer version.
                for(IndexEntry localEntry : localEntries) {
                    for(IndexEntry remoteEntry : remoteEntries) {
                        if(!localEntry.getID().equals(remoteEntry.getID())) {
                            if(showProgressBar) indexCompareBar.step();
                            continue;
                        }
                        filesToDelete.remove(localEntry);
                        if(localEntry.getTimestamp() >= remoteEntry.getTimestamp())
                            filesToDownload.remove(remoteEntry);
                        if(showProgressBar) indexCompareBar.step();
                    }
                }
                if(showProgressBar) indexCompareBar.close();
            } else {
                filesToDownload = remoteIndexFile.getEntries();
            }

            // Download files that should be downloaded
            ProgressBar fileDownloadBar = null;
            if(filesToDownload.size() > 0) {
                indexNeedsUpdate = true;
                if(showProgressBar) {
                    fileDownloadBar = new ProgressBarBuilder()
                            .setTaskName("Downloading files: " + language + "/" + publisher)
                            .setInitialMax(filesToDownload.size())
                            .setMaxRenderedLength(125)
                            .setUpdateIntervalMillis(200)
                            .setStyle(ProgressBarStyle.ASCII)
                            .build();
                }
            }
            for(IndexEntry entry : filesToDownload) {
                String path = language + "/zwi/" + publisher + "/" + entry.getPathInDatabase();

                HttpResponse download = makeRequest(new HttpGet(aggregatorURL + "/db/" + path));
                if(response.getStatusLine().getStatusCode() != 200) continue;

                Path targetPath = root.resolve(path);
                Files.createDirectories(targetPath.getParent());
                Files.write(targetPath, download.getEntity().getContent().readAllBytes());
                if(showProgressBar) fileDownloadBar.step();
            }
            if(showProgressBar && filesToDownload.size() > 0) fileDownloadBar.close();

            // Delete files that should be deleted
            ProgressBar fileDeleteBar = null;
            if(filesToDelete.size() > 0) {
                indexNeedsUpdate = true;
                if(showProgressBar) {
                    fileDeleteBar = new ProgressBarBuilder()
                            .setTaskName("Deleting files: " + language + "/" + publisher)
                            .setInitialMax(filesToDelete.size())
                            .setMaxRenderedLength(125)
                            .setUpdateIntervalMillis(200)
                            .setStyle(ProgressBarStyle.ASCII)
                            .build();
                }
            }
            for(IndexEntry entry : filesToDelete) {
                Path toDelete = root
                        .resolve(language)
                        .resolve("zwi")
                        .resolve(publisher)
                        .resolve(entry.getPathInDatabase());
                ZWIFile toDeleteZWI = new ZWIFile(this, toDelete);
                trashFile(toDeleteZWI, false);
                if(showProgressBar) fileDeleteBar.step();
            }
            if(showProgressBar && filesToDelete.size() > 0) fileDeleteBar.close();
        }
        if(indexNeedsUpdate) {
            buildIndexFiles(showProgressBar);
            buildLuceneIndex(showProgressBar);
        }
        return true;
    }

    /**
     * Updates this {@link ZWIDatabase}'s article count and size stats.
     */
    public void updateInfo() throws IOException {
        AtomicLong localArticleCount = new AtomicLong(0), localSize = new AtomicLong(0);
        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                Files.walkFileTree(publisherPath, new SimpleFileVisitor<>() {

                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if(file.toString().endsWith(".zwi")) {
                            localArticleCount.incrementAndGet();
                            localSize.addAndGet(Files.size(file));
                        }
                        return FileVisitResult.CONTINUE;
                    }

                });
            }
        }
        articleCount = localArticleCount.get();
        size = localSize.get();
    }

    /**
     * @return The number of articles in this {@link ZWIDatabase}.
     *         You may need to call {@link ZWIDatabase#updateInfo()} before calling this method.
     */
    public long getArticleCount() {
        return articleCount;
    }

    /**
     * @return The size of this {@link ZWIDatabase} in bytes.
     *         You may need to call {@link ZWIDatabase#updateInfo()} before calling this method.
     */
    public long getSize() {
        return size;
    }

    /**
     * Can be passed to {@link ZWIDatabase#search(String, int, String, String, String)} to search all fields.
     */
    public static final String SEARCH_ALL_FIELDS = "title,description,content,significantPhrases";

    private static final HashMap<String, Float> BOOSTS = new HashMap<>();
    static {
        BOOSTS.put("title", 50.0f);
        BOOSTS.put("description", 10.0f);
    }

    /**
     * Contains information about a ZWI file that was returned by a search.
     */
    public static class SearchResult {

        private String id, sourceURL, title, description, publisher;

        SearchResult(String id, String sourceURL, String title, String description, String publisher) {
            this.id = id;
            this.sourceURL = sourceURL;
            this.title = title;
            this.description = description;
            this.publisher = publisher;
        }

        /**
         * @hidden
         */
        public SearchResult(JSONObject result) {
            this.id = result.getString("id");
            this.sourceURL = result.getString("sourceURL");
            this.title = result.getString("title");
            this.description = result.getString("description");
            this.publisher = result.getString("publisher");
        }

        /**
         * @return The article's ID
         */
        public String getID() {
            return id;
        }

        /**
         * @return The article's source URL
         */
        public String getSourceURL() {
            return sourceURL;
        }

        /**
         * @return The article's title
         */
        public String getTitle() {
            return title;
        }

        /**
         * @return The article's description. Ideally a short summary of the article, or the first 400 characters.
         */
        public String getDescription() {
            return description;
        }

        /**
         * @return The article's publisher
         */
        public String getPublisher() {
            return publisher;
        }

        /**
         * @hidden
         */
        public JSONObject toJSONObject() {
            JSONObject resultObject = Objects.requireNonNull(Utilities.createOrderedJSONObject());
            resultObject.put("id", id);
            resultObject.put("sourceURL", sourceURL);
            resultObject.put("title", title);
            resultObject.put("description", description);
            resultObject.put("publisher", publisher);
            return resultObject;
        }

    }

    /**
     * Searches this {@link ZWIDatabase}.
     * This method relies on the Lucene index, so you may need to build that first by calling {@link ZWIDatabase#buildLuceneIndex()}.
     * @param keywords The text to search for
     * @return An {@link ArrayList} with up to 150 results
     */
    public ArrayList<SearchResult> search(String keywords) throws IOException, ParseException {
        return search(keywords, 150, SEARCH_ALL_FIELDS, null, null);
    }

    /**
     * Searches this {@link ZWIDatabase}.
     * This method relies on the Lucene index, so you may need to build that first by calling {@link ZWIDatabase#buildLuceneIndex()}.
     * @param keywords The text to search for
     * @param numResults The maximum number of results to return
     * @param fieldsToSearch A comma-separated list of fields to search. Available fields are {@code title}, {@code description}, {@code content}, and {@code significantPhrases}.
     * @param excludedPublishers A comma-separated list of publishers to exclude from the search.
     *                        Either excludedPublishers or includedPublishers must be {@code null}. The list that isn't {@code null} will be used.
     *                        If both fields are {@code null}, all publishers will be included in the search.
     * @param includedPublishers A comma-separated list of publishers to include in the search.
     * @return An {@link ArrayList} of search results
     */
    public ArrayList<SearchResult> search(String keywords,
                                    int numResults,
                                    String fieldsToSearch,
                                    String excludedPublishers,
                                    String includedPublishers) throws IOException, ParseException {

        // If both excludedPublishers and includedPublishers aren't null, throw an exception
        if(excludedPublishers != null && includedPublishers != null) throw new IllegalArgumentException("At least one publisher argument (excludedPublishers or includedPublishers) must be null");

        // Get a list of publishers. If there are no publishers, throw an exception
        ArrayList<String> publishers = getPublishers();
        if(publishers.size() == 0) throw new IllegalStateException("No publishers found");

        // Create the ArrayList of publishers to include in the search
        ArrayList<String> sources = new ArrayList<>();

        // If both excludedPublishers and includedPublishers are null, include all publishers in the search
        if(excludedPublishers == null && includedPublishers == null) sources.addAll(publishers);

        // If excludedPublishers isn't null, exclude the specified encyclopedias from the search
        else if(excludedPublishers != null) {
            sources.addAll(publishers);
            sources.removeAll(List.of(excludedPublishers.split(",")));
        }

        // If includedPublishers isn't null, include only the specified encyclopedias in the search
        else {
            sources.addAll(List.of(includedPublishers.split(",")));
        }

        // If no sources are enabled after doing the above, throw an exception
        if(sources.size() == 0) throw new IllegalStateException("No publishers selected");


        // Load the Lucene index if it hasn't been loaded yet, or it was loaded over 30 minutes ago
        if(searcher == null || System.currentTimeMillis() - lastIndexLoadTime > 1800000) {
            Directory indexDirectory = FSDirectory.open(root.resolve("lucene"));
            searcher = new IndexSearcher(DirectoryReader.open(indexDirectory));
            lastIndexLoadTime = System.currentTimeMillis();
        }

        // Actually get the results
        QueryParser parser = new MultiFieldQueryParser(fieldsToSearch.split(","), new StandardAnalyzer(), BOOSTS);
        TopDocs hits = searcher.search(parser.parse(keywords), numResults);
        ArrayList<SearchResult> results = new ArrayList<>();
        for(int i = 0; i < hits.scoreDocs.length; i++) {
            Document document = searcher.doc(hits.scoreDocs[i].doc);
            if(sources.contains(document.get("publisher"))) { // Exclude excluded publishers from results
                SearchResult result = new SearchResult(
                        document.get("id"),
                        document.get("sourceURL"),
                        document.get("title"),
                        document.get("description"),
                        document.get("publisher")
                );
                results.add(result);
            }
        }
        return results;
    }

    /**
     * @return An {@link ArrayList<String>} of publishers in this {@link ZWIDatabase}.
     */
    public ArrayList<String> getPublishers() throws IOException {
        ArrayList<String> publishers = new ArrayList<>();
        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(Files.isDirectory(publisherPath)) publishers.add(publisherPath.getFileName().toString());
            }
        }
        return publishers;
    }

    /**
     * Updates this {@link ZWIDatabase}'s index files and Lucene index.
     */
    public void buildAllIndexes() throws IOException {
        buildIndexFiles(false);
        buildLuceneIndex(false);
    }

    /**
     * Updates this {@link ZWIDatabase}'s Lucene index.
     */
    public void buildLuceneIndex() throws IOException {
        buildLuceneIndex(false);
    }

    /**
     * Updates this {@link ZWIDatabase}'s Lucene index.
     * @param showProgressBar Whether to show an ASCII progress bar
     */
    public void buildLuceneIndex(boolean showProgressBar) throws IOException {
        updateInfo();
        ZWIDatabase thisDatabase = this;
        AtomicReference<ProgressBar> progressBar = new AtomicReference<>();
        if(showProgressBar) progressBar.set(Utilities.createProgressBar("Building Lucene index", getArticleCount() + 2));
        IndexWriter writer = new IndexWriter(FSDirectory.open(root.resolve("luceneTemp").toAbsolutePath()), getIndexWriterConfig());
        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(!Files.isDirectory(publisherPath)) continue;
                Files.walkFileTree(publisherPath, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if(!file.toString().toLowerCase().endsWith(".zwi")) return FileVisitResult.CONTINUE; // Ignore non-ZWI files

                        ZWIFile zwiFile = new ZWIFile(thisDatabase, file);
                        ZWIMetadata metadata = zwiFile.getMetadata();
                        Document document = new Document();

                        document.add(new TextField("id", metadata.getID(), Field.Store.YES));
                        document.add(new TextField("sourceURL", metadata.getSourceURL(), Field.Store.YES));
                        document.add(new TextField("publisher", metadata.getPublisher(), Field.Store.YES));
                        document.add(new TextField("title", metadata.getTitle().replace("_", " "), Field.Store.YES));
                        document.add(new TextField("description", metadata.getDescription(), Field.Store.YES));
                        if(metadata.hasSignificantPhrases())
                            document.add(new TextField("significantPhrases", String.join(" ", metadata.getSignificantPhrases()), Field.Store.YES));
                        byte[] content = Utilities.getFileContentsInZWI(Files.readAllBytes(file), "article.txt");
                        if(content != null) document.add(new TextField("content", new String(content), Field.Store.YES));

                        writer.addDocument(document);

                        if(showProgressBar) progressBar.get().step();
                        return FileVisitResult.CONTINUE;
                    }
                });
            }
        }

        writer.close();
        progressBar.get().step();

        Utilities.deleteDirectory(root.resolve("lucene"));
        Files.move(root.resolve("luceneTemp"), root.resolve("lucene"));
        progressBar.get().step();

        if(showProgressBar) progressBar.get().close();
    }

    /**
     * Updates this {@link ZWIDatabase}'s index files.
     */
    public void buildIndexFiles() throws IOException {
        buildIndexFiles(false);
    }

    /**
     * Updates this {@link ZWIDatabase}'s index files.
     * @param showProgressBar Whether to show an ASCII progress bar
     */
    public void buildIndexFiles(boolean showProgressBar) throws IOException {
        updateInfo();
        ZWIDatabase thisDatabase = this;
        AtomicReference<ProgressBar> progressBar = new AtomicReference<>();
        if(showProgressBar) progressBar.set(new ProgressBarBuilder()
                .setTaskName("Building index files")
                .setInitialMax(getArticleCount())
                .setMaxRenderedLength(125)
                .setUpdateIntervalMillis(200)
                .setStyle(ProgressBarStyle.ASCII)
                .build());

        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(!Files.isDirectory(publisherPath)) continue;

                Path indexLocation = publisherPath.resolve("index.csv.gz");
                IndexFile indexFile = new IndexFile();
                indexFile.setLocation(indexLocation); // I don't use new IndexFile(indexLocation) because that loads the contents of the old file

                Files.walkFileTree(publisherPath, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if(!file.toString().toLowerCase().endsWith(".zwi")) return FileVisitResult.CONTINUE; // Ignore non-ZWI files
                        indexFile.addEntry(new ZWIFile(thisDatabase, file), false);
                        if(showProgressBar) progressBar.get().step();
                        return FileVisitResult.CONTINUE;
                    }
                });

                Files.deleteIfExists(indexLocation); // Delete the old index file
                indexFile.write(); // Create the new index file
            }
        }
        if(showProgressBar) progressBar.get().close();
    }

    /**
     * Moves all files in this {@link ZWIDatabase} to the correct folders, and builds indexes.
     * This method is used to convert old ZWI databases to the new format.
     */
    public void migrate(boolean showProgressBar) throws IOException {
        updateInfo();
        ZWIDatabase thisDatabase = this;
        AtomicReference<ProgressBar> progressBar = new AtomicReference<>();
        if(showProgressBar) progressBar.set(new ProgressBarBuilder()
                .setTaskName("Migrating database")
                .setInitialMax(getArticleCount())
                .setMaxRenderedLength(125)
                .setUpdateIntervalMillis(200)
                .setStyle(ProgressBarStyle.ASCII)
                .build());
        for(Path path : getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(!Files.isDirectory(publisherPath)) continue;

                Files.walkFileTree(publisherPath, new SimpleFileVisitor<>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                        if(!file.toString().toLowerCase().endsWith(".zwi")) return FileVisitResult.CONTINUE; // Ignore non-ZWI files

                        ZWIFile zwiFile = new ZWIFile(thisDatabase, file);
                        ZWIMetadata zwiFileMetadata;
                        try {
                            zwiFileMetadata = zwiFile.getMetadata();
                        } catch(IllegalStateException | ZipException | JSONException e) {
                            Files.createDirectories(path.resolve("skipped"));
                            Files.move(
                                zwiFile.getLocation(),
                                path.resolve("skipped").resolve(Utilities.getSHA1Hash(Files.readAllBytes(zwiFile.getLocation())) + ".zwi"),
                                StandardCopyOption.REPLACE_EXISTING
                            );
                            return FileVisitResult.CONTINUE;
                        }
                        String filename = zwiFileMetadata.getID() + ".zwi";

                        if(file.relativize(publisherPath).toString().equals(zwiFile.getPartialPath())
                        && file.getFileName().toString().equals(filename))
                            return FileVisitResult.CONTINUE; // Ignore files that have already been processed or that don't need processing

                        // file = the file we're processing
                        // targetDir = the folder we're putting the file in
                        // targetPath = targetDir + file - the destination of the file
                        Path targetDir = publisherPath.resolve(zwiFile.getPartialPath());
                        Path targetPath = targetDir.resolve(filename);

                        Files.createDirectories(targetDir);

                        // Try to move the file
                        try {
                            Files.move(file, targetPath);

                        // If there's another file in the destination folder with the same name...
                        } catch(FileAlreadyExistsException e) {

                            // If the files are the same, delete the file we're trying to move
                            if(Utilities.isSameFile(file, targetPath)) {
                                Files.delete(file);

                            // Otherwise...
                            } else {

                                // Determine which of the files is newer
                                ZWIFile otherFile = new ZWIFile(targetPath);
                                ZWIMetadata otherFileMetadata;
                                try {
                                    otherFileMetadata = otherFile.getMetadata();
                                } catch(IllegalStateException e2) {
                                    Files.createDirectories(path.resolve("skipped"));
                                    Files.move(
                                            otherFile.getLocation(),
                                            path.resolve("skipped").resolve(Utilities.getSHA1Hash(Files.readAllBytes(otherFile.getLocation())) + ".zwi"),
                                            StandardCopyOption.REPLACE_EXISTING
                                    );
                                    return FileVisitResult.CONTINUE;
                                }

                                long zwiFileTimestamp = zwiFileMetadata.getLastModifiedEpochSeconds();
                                long otherFileTimestamp = otherFileMetadata.getLastModifiedEpochSeconds();
                                boolean otherFileNewer = otherFileTimestamp >= zwiFileTimestamp;

                                // Move the older file to the trash
                                if(otherFileNewer) trashFile(zwiFile);
                                else trashFile(otherFile);
                            }
                        }

                        if(showProgressBar) progressBar.get().step();
                        return FileVisitResult.CONTINUE;
                    }
                });
            }
        }
        if(showProgressBar) progressBar.get().close();
        buildIndexFiles(showProgressBar);
        buildLuceneIndex(showProgressBar);
    }

    /**
     * @hidden
     */
    public List<Path> getLanguages() throws IOException {
        return Files.list(root).filter(path -> Language.isValid(path.getFileName().toString())).toList();
    }

}
