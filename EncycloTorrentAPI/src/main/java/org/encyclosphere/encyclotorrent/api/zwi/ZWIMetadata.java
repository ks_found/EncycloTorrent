package org.encyclosphere.encyclotorrent.api.zwi;

import org.encyclosphere.encyclotorrent.api.misc.Language;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;

/**
 * Contains most of the information in a ZWI file's {@code metadata.json}:
 * things like the article title, description, and source URL.
 */
public class ZWIMetadata {

    private String title, id, description, comment, sourceURL, publisher, license;

    private Language language;

    private final ArrayList<String> categories = new ArrayList<>(),
                            significantPhrases = new ArrayList<>();

    private LocalDateTime lastModified, timeCreated;


    /**
     * Creates an empty {@link ZWIMetadata} object.
     */
    public ZWIMetadata() {
        title = id = description = comment = sourceURL = publisher = license = "";
        language = Language.ENGLISH;
        lastModified = timeCreated = LocalDateTime.now();
    }

    /**
     * Creates a {@link ZWIMetadata} object.
     * @param metadata The metadata of a ZWI file, as a {@link JSONObject}
     */
    public ZWIMetadata(JSONObject metadata) {
        double zwiVersion = metadata.getDouble("ZWIversion");
        if(zwiVersion != 1.3) throw new IllegalStateException("Unsupported ZWI version: " + zwiVersion);
        title = metadata.getString("Title");
        id = ZWIFile.getID(metadata.getString("SourceURL"));
        language = Language.fromCode(metadata.getString("Lang"));
        publisher = metadata.getString("Publisher").strip();
        lastModified = stringToLocalDateTime(metadata.getString("LastModified"));
        timeCreated = stringToLocalDateTime(metadata.getString("TimeCreated"));
        for(int i = 0; i < metadata.getJSONArray("Categories").length(); i++) categories.add(metadata.getJSONArray("Categories").getString(i));
        if(metadata.has("Description")) description = metadata.getString("Description");
        else description = "";
        if(metadata.has("Comment")) comment = metadata.getString("Comment");
        else comment = "";
        if(metadata.has("SignificantPhrases")) {
            JSONArray significantPhrasesArray = metadata.getJSONArray("SignificantPhrases");
            for(int i = 0; i < significantPhrasesArray.length(); i++) categories.add(significantPhrasesArray.getString(i));
        }
        license = metadata.getString("License");
        sourceURL = metadata.getString("SourceURL");
    }

    /**
     * Creates a {@link ZWIMetadata} object.
     * @param zwiContents The contents of a ZWI file, <b>not</b> the metadata itself, as a byte array
     * @return A {@link ZWIMetadata} object containing the metadata of the ZWI file
     */
    public static ZWIMetadata fromZWIBytes(byte[] zwiContents) throws IOException {
        byte[] metadataBytes = Utilities.getFileContentsInZWI(zwiContents, "metadata.json");
        if(metadataBytes == null) return null;
        return new ZWIMetadata(new JSONObject(new String(metadataBytes)));
    }



    private LocalDateTime stringToLocalDateTime(String str) {
        return Instant.ofEpochSecond(Integer.parseInt(str)).atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    private long localDateTimeToEpochSeconds(LocalDateTime date) {
        return date.toEpochSecond(ZoneOffset.UTC);
    }


    /**
     * @return The title of the article
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the title of the article.
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The ID of the article, which is the SHA-256 hash of the source URL with trailing slashes removed
     */
    public String getID() {
        return id;
    }

    /**
     * @return The article's description. Ideally a short summary of the article, or the first 400 characters.
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the article's description.
     * @param description The description. Ideally a short summary of the article, or the first 400 characters.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Some additional information about the article
     */
    public String getComment() {
        return comment;
    }

    /**
     * Sets the article's comment.
     * @param comment Some additional information about the article
     */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
     * @return Whether this {@link ZWIMetadata} has significant phrases
     */
    public boolean hasSignificantPhrases() {
        return significantPhrases.isEmpty();
    }

    /**
     * @return An {@link ArrayList} of significant phrases from the article's content
     */
    public ArrayList<String> getSignificantPhrases() {
        return significantPhrases;
    }

    /**
     * @return The article's source URL
     */
    public String getSourceURL() {
        return sourceURL;
    }

    /**
     * Sets the article's source URL.
     * @param url The source URL
     */
    public void setSourceURL(String url) {
        this.sourceURL = url;
        this.id = ZWIFile.getID(sourceURL);
    }

    /**
     * @return The article's publisher
     */
    public String getPublisher() {
        return publisher;
    }

    /**
     * Sets the article's publisher.
     * @param publisher The publisher
     */
    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    /**
     * @return The article's license
     */
    public String getLicense() {
        return license;
    }

    /**
     * Sets the article's license.
     * @param license The license
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return The {@link Language} the article is in
     */
    public Language getLanguage() {
        return language;
    }

    /**
     * @return The 2-letter code representing the language the article is in
     */
    public String getLanguageCode() {
        return language.getCode();
    }

    /**
     * Sets the article's language.
     * @param language The {@link Language} the article is in
     */
    public void setLanguage(Language language) {
        this.language = language;
    }

    /**
     * @return An {@link ArrayList} of the article's categories
     */
    public ArrayList<String> getCategories() {
        return categories;
    }

    /**
     * @return The last time the article was modified, as a {@link LocalDateTime}
     */
    public LocalDateTime getLastModified() {
        return lastModified;
    }

    /**
     * @return The last time the article was modified, in Unix epoch format (seconds since the Unix epoch)
     */
    public long getLastModifiedEpochSeconds() {
        return localDateTimeToEpochSeconds(lastModified);
    }

    /**
     * Sets the last time the article was modified.
     * @param lastModified The last time the article was modified
     */
    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @return The time the article was created, as a {@link LocalDateTime}
     */
    public LocalDateTime getTimeCreated() {
        return timeCreated;
    }

    /**
     * @return The time the article was created, in Unix epoch format (seconds since the Unix epoch)
     */
    public long getTimeCreatedEpochSeconds() {
        return localDateTimeToEpochSeconds(timeCreated);
    }

    /**
     * Sets the time the article was created.
     * @param timeCreated The time the article was created
     */
    public void setTimeCreated(LocalDateTime timeCreated) {
        this.timeCreated = timeCreated;
    }

}
