package org.encyclosphere.encyclotorrent.api.zwi;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.FileHeader;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.encyclosphere.encyclotorrent.api.Downloader;
import org.encyclosphere.encyclotorrent.api.index.IndexFile;
import org.encyclosphere.encyclotorrent.api.misc.TorrentClient;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collections;
import java.util.List;

/**
 * Represents a ZWI file on disk.
 */
public class ZWIFile {

    private final Path location;

    private final ZWIDatabase database;

    private ZWIData data;

    private boolean hasMetadata;



    /**
     * Creates a {@link ZWIFile} object.
     * @param location The {@link Path} to the file. If the file is in a database, use {@link ZWIFile#ZWIFile(ZWIDatabase, Path)}.
     */
    public ZWIFile(Path location) {
        this.database = null;
        this.location = location;
        this.data = new ZWIData();
    }

    /**
     * Creates a {@link ZWIFile} object.
     * @param database The {@link ZWIDatabase} the file is in. If the file isn't in a database, use {@link ZWIFile#ZWIFile(Path)}.
     * @param location The {@link Path} to the file
     */
    public ZWIFile(ZWIDatabase database, Path location) {
        this.database = database;
        this.location = location;
        this.data = new ZWIData();
    }

    // Used internally by ZWIData
    ZWIFile(ZWIDatabase database, Path location, ZWIData data) {
        this.database = database;
        this.location = location;
        this.data = data;
    }



    /**
     * @return The location of this {@link ZWIFile} as a {@link Path}
     */
    public Path getLocation() {
        return location;
    }

    /**
     * @return The {@link ZWIDatabase} this {@link ZWIFile} is in, or {@code null} if it isn't in a database
     */
    public ZWIDatabase getDatabase() {
        return database;
    }

    public ZWITorrent getTorrent() throws IOException {
        return getTorrent(Collections.emptyList());
    }

    public ZWITorrent getTorrent(List<String> trackers) throws IOException {
        String filename = getMetadata().getTitle() + ".zwi.torrent";
        byte[] content = TorrentClient.createTorrent(this, trackers, Downloader.getDownloadLinks(getID(), getMetadata().getLanguage(), getMetadata().getPublisher()));
        return new ZWITorrent(filename, content);
    }

    /**
     * @return The {@link IndexFile} in this {@link ZWIFile}'s parent database which contains information about the file,
     *         or {@code null} if an index file with information about the ZWI couldn't be found
     * @throws UnsupportedOperationException If this {@code ZWIFile} isn't in a database
     */
    public IndexFile getIndexFile() throws IOException {
        if(!isInDatabase()) throw new UnsupportedOperationException("File is not in a database");
        Path indexPath = database.getRoot()
                .resolve(getMetadata().getLanguageCode())
                .resolve("zwi")
                .resolve(getMetadata().getPublisher())
                .resolve("index.csv.gz");
        if(Files.notExists(indexPath)) database.buildIndexFiles(false);
        return new IndexFile(indexPath);
    }

    /**
     * @return Part of the path to this {@link ZWIFile} in a {@link ZWIDatabase}.
     */
    public String getPartialPath() throws IOException {
        return Utilities.getPartialPath(getID());
    }

    /**
     * @return This {@link ZWIFile}'s {@link ZWIData}.
     * @throws IOException
     */
    public ZWIData getData() throws IOException {
        if(data.isMetadataOnly()) data.populate(Files.readAllBytes(location));
        return data;
    }

    public void setData(ZWIData data) {
        this.data = data;
    }

    /**
     * @return A Lucene {@link Document} containing the metadata and text content of this {@link ZWIFile}.
     */
    public Document getLuceneDocument() throws IOException {
        ZWIMetadata metadata = getMetadata();
        Document document = new Document();

        document.add(new TextField("id", metadata.getID(), Field.Store.YES));
        document.add(new TextField("publisher", metadata.getPublisher(), Field.Store.YES));
        document.add(new TextField("title", metadata.getTitle(), Field.Store.YES));
        document.add(new TextField("description", metadata.getDescription(), Field.Store.YES));
        if(metadata.hasSignificantPhrases())
            document.add(new TextField("significantPhrases", String.join(" ", metadata.getSignificantPhrases()), Field.Store.YES));
        byte[] content = Utilities.getFileContentsInZWI(Files.readAllBytes(location), "article.txt");
        if(content != null) document.add(new TextField("content", new String(content), Field.Store.YES));

        return document;
    }

    /**
     * @return This {@link ZWIFile}'s metadata
     */
    public ZWIMetadata getMetadata() throws IOException {
        if(!hasMetadata) {
            data.setMetadata(new ZWIMetadata(getJSONMetadata()));
            hasMetadata = true;
        }
        return data.getMetadata();
    }

    /**
     * @return The metadata of the ZWI file, as a {@link JSONObject}.
     */
    public JSONObject getJSONMetadata() throws IOException {
        ZipFile zwiFile = new ZipFile(location.toAbsolutePath().toString());
        FileHeader header = zwiFile.getFileHeader("metadata.json");
        InputStream metadataStream = zwiFile.getInputStream(header);
        return new JSONObject(new String(metadataStream.readAllBytes()));
    }

    /**
     * @return The ID of the ZWI file.
     */
    public String getID() throws IOException {
        return getMetadata().getID();
    }

    /**
     * @return Whether this {@link ZWIFile} is in a database
     */
    public boolean isInDatabase() {
        return database != null;
    }



    /**
     * Gets the ID of a ZWI file from its URL.
     * @param sourceURL A URL
     * @return The corresponding ID
     */
    public static String getID(String sourceURL) {
        if(sourceURL.endsWith("/")) sourceURL = sourceURL.substring(0, sourceURL.length() - 1);  // Remove trailing slash if present
        return Utilities.getSHA256Hash(sourceURL.getBytes());                                    // Return the SHA-256 hash of the URL
    }

    /**
     * Checks if a {@link ZWIFile} is valid.
     * @param zwiFile The file to check
     * @return Whether the file is a valid ZWI file
     * @throws FileNotFoundException If the file doesn't exist
     */
    public static boolean isValid(ZWIFile zwiFile) throws FileNotFoundException {
        return isValid(zwiFile.getLocation());
    }

    /**
     * Checks if a file is a valid ZWI file.
     * @param path The file to check
     * @return Whether the file is a valid ZWI file
     * @throws FileNotFoundException If the file doesn't exist
     */
    public static boolean isValid(Path path) throws FileNotFoundException {
        if(Files.notExists(path)) throw new FileNotFoundException(path.toAbsolutePath().toString());
        if(Files.isDirectory(path)) return false;

        try {
            return ZWIFile.isValid(Files.readAllBytes(path));
        } catch(IOException e) {
            return false;
        }
    }

    /**
     * Checks if a byte array is valid ZWI data.
     * @param zwiData The data to check
     * @return Whether the byte array is valid ZWI data
     */
    public static boolean isValid(byte[] zwiData) {
        try {
            // TODO improve this
            new ZWIData(zwiData);
            return true;
        } catch(Exception e) {
            return false;
        }
    }

}
