package org.encyclosphere.encyclotorrent.api.zwi;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.io.inputstream.ZipInputStream;
import net.lingala.zip4j.model.LocalFileHeader;
import net.lingala.zip4j.model.ZipParameters;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Contains the contents of a ZWI file.
 */
public class ZWIData {

    private ZWIMetadata metadata;

    private boolean metadataOnly;

    private String primaryArticleFile = "";

    private final ArrayList<ZWIContent> articleFiles = new ArrayList<>(),
                                              images = new ArrayList<>(),
                                              videos = new ArrayList<>(),
                                               audio = new ArrayList<>(),
                                                 css = new ArrayList<>();



    /**
     * Creates an empty {@link ZWIData} object.
     */
    public ZWIData() {
        metadata = new ZWIMetadata();
    }

    /**
     * Creates a {@link ZWIData} object.
     * @param zwiBytes The contents of a ZWI file as a byte array
     */
    public ZWIData(byte[] zwiBytes) throws IOException {
        this.populate(zwiBytes);
    }



    void populate(byte[] zwiBytes) throws IOException {
        ZipInputStream zwiStream = new ZipInputStream(new ByteArrayInputStream(zwiBytes));
        LocalFileHeader header;
        while((header = zwiStream.getNextEntry()) != null) {
            String pathInZip = header.getFileName();
            int lastSlashIndex = pathInZip.lastIndexOf('/');
            String filename = pathInZip;
            if(lastSlashIndex != -1) filename = filename.substring(lastSlashIndex + 1);

            if(pathInZip.equals("metadata.json")) {
                JSONObject metadataObject = new JSONObject(new String(zwiStream.readAllBytes()));
                primaryArticleFile = metadataObject.getString("Primary");
                if(!primaryArticleFile.strip().equals("") && getPrimaryArticleFile() == null)
                    throw new IllegalStateException("Invalid ZWI file: Primary article file specified in metadata.json does not exist");

                metadata = new ZWIMetadata(metadataObject);
            } else if(pathInZip.startsWith("article.")) {
                ZWIContent articleFile = new ZWIContent();
                articleFile.setFilename(filename);
                articleFile.setContent(zwiStream.readAllBytes());
                articleFiles.add(articleFile);
            } else if(pathInZip.startsWith("data/media/images") && !filename.isBlank()) {
                ZWIContent image = new ZWIContent();
                image.setFilename(filename);
                image.setContent(zwiStream.readAllBytes());
                images.add(image);
            } else if(pathInZip.startsWith("data/media/videos") && !filename.isBlank()) {
                ZWIContent video = new ZWIContent();
                video.setFilename(filename);
                video.setContent(zwiStream.readAllBytes());
                videos.add(video);
            } else if(pathInZip.startsWith("data/media/audio") && !filename.isBlank()) {
                ZWIContent audioFile = new ZWIContent();
                audioFile.setFilename(filename);
                audioFile.setContent(zwiStream.readAllBytes());
                audio.add(audioFile);
            } else if(pathInZip.startsWith("data/css") && !filename.isBlank()) {
                ZWIContent cssFile = new ZWIContent();
                cssFile.setFilename(filename);
                cssFile.setContent(zwiStream.readAllBytes());
                css.add(cssFile);
            }
        }
        if(metadata == null) throw new IllegalStateException("Invalid ZWI file: Missing metadata.json");
        this.metadataOnly = false;
    }

    /**
     * @return The primary article file, or {@code null} if there is no primary article file
     */
    public ZWIContent getPrimaryArticleFile() {
        if(primaryArticleFile.equals("")) return null;
        for(ZWIContent articleFile : articleFiles) {
            if(articleFile.getFilename().equals(primaryArticleFile)) return articleFile;
        }
        return null;
    }

    /**
     * Adds an article file to this {@link ZWIData}, and makes it the primary article file.
     * @param primaryArticleFile The article file to add
     */
    public void setPrimaryArticleFile(ZWIContent primaryArticleFile) {
        articleFiles.add(primaryArticleFile);
        this.primaryArticleFile = primaryArticleFile.getFilename();
    }

    /**
     * @return The filename of the primary article file
     */
    public String getPrimaryArticleFilename() {
        return primaryArticleFile;
    }

    /**
     * Sets the filename of the primary article file.
     * @param primaryArticleFile The filename
     * @throws IllegalArgumentException If there is no article file with the given filename
     */
    public void setPrimaryArticleFilename(String primaryArticleFile) {
        boolean hasArticleFile = false;
        for(ZWIContent articleFile : articleFiles) {
            if(articleFile.getFilename().equals(primaryArticleFile)) {
                hasArticleFile = true;
                break;
            }
        }
        if(hasArticleFile) this.primaryArticleFile = primaryArticleFile;
        else throw new IllegalArgumentException("This ZWIData doesn't contain an article with the given filename");
    }

    /**
     * @return An {@link ArrayList<ZWIContent>} of article files in this {@link ZWIData}
     */
    public ArrayList<ZWIContent> getArticleFiles() {
        return articleFiles;
    }

    /**
     * @return An {@link ArrayList<ZWIContent>} of images in this {@link ZWIData}
     */
    public ArrayList<ZWIContent> getImages() {
        return images;
    }

    /**
     * @return An {@link ArrayList<ZWIContent>} of videos in this {@link ZWIData}
     */
    public ArrayList<ZWIContent> getVideos() {
        return videos;
    }

    /**
     * @return An {@link ArrayList<ZWIContent>} of audio files in this {@link ZWIData}
     */
    public ArrayList<ZWIContent> getAudio() {
        return audio;
    }

    /**
     * @return An {@link ArrayList<ZWIContent>} of CSS files in this {@link ZWIData}
     */
    public ArrayList<ZWIContent> getCSSFiles() {
        return css;
    }


    /**
     * @return The metadata of this {@link ZWIData}
     */
    public ZWIMetadata getMetadata() {
        return metadata;
    }

    /**
     * Sets this {@link ZWIData}'s metadata.
     * @param metadata The metadata
     */
    public void setMetadata(ZWIMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * @return Whether this {@link ZWIData} object contains only metadata.
     *         This method is used internally by {@link ZWIFile#getData()}.
     */
    public boolean isMetadataOnly() {
        return metadataOnly;
    }


    /**
     * Writes this {@link ZWIData} to a ZWI file on disk.
     * If you're adding the file to a database, use {@link ZWIData#buildZWIFile(ZWIDatabase)}.
     * @param path The {@link Path} to the file to write to
     * @return The file on disk, as a {@link ZWIFile}
     * @throws IOException If the file already exists or couldn't be written to
     */
    public ZWIFile buildZWIFile(Path path) throws IOException {
        return buildZWIFile(null, path);
    }

    /**
     * Writes this {@link ZWIData} to a ZWI file on disk, and adds it to a database.
     * Note that this method does not update the database's indexes.<br>
     * If you're adding a single file to the database, call {@link ZWIDatabase#addToIndexes(ZWIFile)} on the result of this method.<br>
     * If you're adding files to the database in bulk, call {@link ZWIDatabase#buildAllIndexes()} when you're done.
     * @param database The {@link ZWIDatabase} to put the file in
     * @return The file in the database, as a {@link ZWIFile}
     * @throws IOException If the file already exists or couldn't be written to
     */
    public ZWIFile buildZWIFile(ZWIDatabase database) throws IOException {
        return buildZWIFile(database, Utilities.getPathInDatabase(database, metadata));
    }

    private ZWIFile buildZWIFile(ZWIDatabase database, Path path) throws IOException {

        // Make sure the file doesn't already exist
        if(Files.exists(path)) throw new FileAlreadyExistsException(path.toAbsolutePath().toString());

        // Create the ZWI file's parent directories if they don't exist
        Files.createDirectories(path.getParent());

        // Create a ZipFile object, so we can add files to the ZWI
        ZipFile zwiZip = new ZipFile(path.toFile());

        // Add article files and media
        for(ZWIContent articleFile : articleFiles) writeZipEntry(zwiZip, articleFile.getFilename(), articleFile.getContent());
        for(ZWIContent image : images) writeZipEntry(zwiZip, "data/media/images/" + image.getFilename(), image.getContent());
        for(ZWIContent video : videos) writeZipEntry(zwiZip, "data/media/videos/" + video.getFilename(), video.getContent());
        for(ZWIContent audio : audio) writeZipEntry(zwiZip, "data/media/audio/" + audio.getFilename(), audio.getContent());
        for(ZWIContent css : css) writeZipEntry(zwiZip, "data/css/" + css.getFilename(), css.getContent());

        // Add media.json
        if(images.size() > 0) {
            JSONObject media = new JSONObject();
            for(ZWIContent image : images) media.put("data/media/images/" + image.getFilename(), image.getSHA1Hash());
            writeZipEntry(zwiZip, "media.json", media.toString(4).getBytes());
        }

        // Add metadata.json
        JSONObject metadataObject = Objects.requireNonNull(Utilities.createOrderedJSONObject());
        metadataObject.put("ZWIversion", 1.3)
                      .put("Title", metadata.getTitle())
                      .put("Lang", metadata.getLanguageCode());

        JSONObject contentHashes = new JSONObject();
        for(ZWIContent articleFile : articleFiles) contentHashes.put(articleFile.getFilename(), articleFile.getSHA1Hash());

        metadataObject.put("Content", contentHashes)
                      .put("Primary", primaryArticleFile)
                      .put("Revisions", new JSONArray())
                      .put("Publisher", metadata.getPublisher())
                      .put("CreatorNames", new JSONArray())
                      .put("ContributorNames", new JSONArray()).put("LastModified", String.valueOf(Instant.now().getEpochSecond()))
                      .put("LastModified", String.valueOf(LocalDateTime.now().atZone(ZoneOffset.UTC).toInstant().getEpochSecond()))
                      .put("TimeCreated", String.valueOf(metadata.getTimeCreated().atZone(ZoneOffset.UTC).toInstant().getEpochSecond()))
                      .put("Categories", new JSONArray(metadata.getCategories()))
                      .put("Topics", new JSONArray())
                      .put("Rating", new JSONArray())
                      .put("Description", metadata.getDescription())
                      .put("Comment", metadata.getComment());

        if(!metadata.getSignificantPhrases().isEmpty()) metadataObject.put("SignificantPhrases", metadata.getSignificantPhrases());

        metadataObject.put("License", metadata.getLicense())
                      .put("SourceURL", metadata.getSourceURL());
        writeZipEntry(zwiZip, "metadata.json", metadataObject.toString(4).getBytes());

        zwiZip.close();

        return new ZWIFile(database, path, this);
    }

    private static void writeZipEntry(ZipFile zwiFile, String filename, byte[] content) throws ZipException {
        ZipParameters params = new ZipParameters();
        params.setFileNameInZip(filename);
        zwiFile.addStream(new ByteArrayInputStream(content), params);
    }

}
