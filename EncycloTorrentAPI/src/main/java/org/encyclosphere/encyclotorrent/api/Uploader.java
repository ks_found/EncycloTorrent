package org.encyclosphere.encyclotorrent.api;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.log;
import static org.encyclosphere.encyclotorrent.api.misc.Utilities.makeRequest;

/**
 * Contains various methods for uploading files to aggregators.
 */
public class Uploader {

    /**
     * Uploads a {@link ZWIFile} to the Encyclosphere.
     * @param file The file to upload
     * @return The number of aggregators the file was uploaded to
     */
    public static int uploadToAggregators(ZWIFile file) {
        return uploadToAggregators(file, null);
    }

    /**
     * Uploads a {@link ZWIFile} to the Encyclosphere.
     * @param file The file to upload
     * @param passwords A {@link HashMap} of aggregator URLs and passwords.
     *                        If passwords aren't required, use {@link Uploader#uploadToAggregators(ZWIFile)}.
     * @return The number of aggregators the file was uploaded to
     */
    public static int uploadToAggregators(ZWIFile file, HashMap<String, String> passwords) {
        if(passwords == null) passwords = new HashMap<>();
        int successfulUploads = 0;
        for(String aggregatorURL : Aggregators.getAggregators()) {
            boolean success = uploadToAggregator(file, aggregatorURL, passwords.get(aggregatorURL));
            if(success) successfulUploads++;
        }
        return successfulUploads;
    }

    /**
     * Uploads a {@link ZWIFile} to an aggregator.
     * @param file The file to upload
     * @param aggregatorURL The aggregator to upload the file to
     * @return {@code true} if the article was uploaded successfully, {@code false} otherwise
     */
    public static boolean uploadToAggregator(ZWIFile file, String aggregatorURL) {
        return uploadToAggregator(file, aggregatorURL, null);
    }

    /**
     * Uploads a {@link ZWIFile} to an aggregator.
     * @param file The file to upload
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @param password The password. If a password isn't required, use {@link Uploader#uploadToAggregator(ZWIFile, String)}.
     * @return {@code true} if the article was uploaded successfully, {@code false} otherwise
     */
    public static boolean uploadToAggregator(ZWIFile file, String aggregatorURL, String password) {
        boolean success = false;
        String filename = file.getLocation().getFileName().toString();
        try {
            log("Uploading " + filename + " (enc://" + file.getID() + ") to " + aggregatorURL + "...");

            HttpEntity entity = MultipartEntityBuilder.create().addPart("file", new FileBody(file.getLocation().toFile())).build();

            HttpPost request = new HttpPost(aggregatorURL + "/upload");
            request.setEntity(entity);

            if(password != null)
                request.addHeader("Authorization", "Basic " + new String(Base64.getEncoder().encode(password.getBytes())));

            HttpResponse response = makeRequest(request);
            if(response.getStatusLine().getStatusCode() == 200) {
                success = true;
            } else if(response.getStatusLine().getStatusCode() != 500) {
                JSONObject errorObject = new JSONObject(new String(response.getEntity().getContent().readAllBytes()));
                String errorMessage = errorObject.getString("message");
                log("Failed to upload " + filename + " to " + aggregatorURL + ": " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase() + " (" + errorMessage + ")");
            } else {
                log("Failed to upload " + filename + " to " + aggregatorURL + ": " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            }
        } catch(IOException e) {
            Utilities.logException("Failed to upload " + filename + " to " + aggregatorURL, e);
        }
        return success;
    }

}
