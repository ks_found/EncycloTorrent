package org.encyclosphere.encyclotorrent.api.test;

import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

import static org.encyclosphere.encyclotorrent.api.test.TestUtilities.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ZWIDatabaseTest {

    static ZWIDatabase database;

    static ZWIFile testFile;

    @BeforeAll
    static void setUpDatabase() throws IOException {
        database = new ZWIDatabase(Path.of("testTemp/database"));
    }

    @Test
    public void testFileAdd() throws Exception {
        testFile = database.addFile(FIRST_ZWI_PATH);
        assertTrue(Files.exists(testFile.getLocation()));
        // TODO Expand test; test index
    }

    @Test
    public void testFileDelete() throws IOException {
        database.deleteFile(testFile);
        assertTrue(Files.notExists(testFile.getLocation()));
        // TODO Expand test; test index
    }

    @Test
    public void testArticleCount() {
        assertEquals(1, database.getArticleCount());
    }

    @AfterAll
    static void cleanUp() throws IOException {
        if(DELETE_TEMP_ON_TEST_FINISH) {
            //noinspection ResultOfMethodCallIgnored
            Files.walk(Path.of("testTemp"))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        }
    }

}
