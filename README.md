# EncycloTorrent
A work-in-progress Java API and client for the [Encyclosphere](https://encyclosphere.org).

The client allows you to do the following:
- Run an aggregator, a server that hosts a database of [ZWI files](https://gitlab.com/ks_found/ZWISpec)
- Manipulate databases of ZWI files
- Download ZWI files from the Encyclosphere using BitTorrent
- Upload ZWI files to the Encyclosphere or to individual aggregators
- Sync a local database of ZWI files with an aggregator's database

For more information, check out the [wiki](https://gitlab.com/ks_found/EncycloTorrent/-/wikis/Home).

For installation instructions, refer to [this wiki page](https://gitlab.com/ks_found/EncycloTorrent/-/wikis/Installation).
