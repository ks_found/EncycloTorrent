package org.encyclosphere.encyclotorrent.api.aggregator;

import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.WelcomeServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.download.GetURLServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.download.TorrentDownloadServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.download.ZWIDownloadServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.download.ZWIMetadataServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.info.AggregatorListServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.info.IndexListServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.info.InfoServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.info.SearchServlet;
import org.encyclosphere.encyclotorrent.api.aggregator.servlets.upload.UploadServlet;
import org.encyclosphere.encyclotorrent.api.misc.ExceptionHandler;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.*;

/**
 * An HTTP server that hosts a {@link ZWIDatabase}, providing a REST API and direct download links for ZWI files.
 */
public class Aggregator {

    public static final String API_VERSION = "1.0";


    private final ZWIDatabase database;

    private final Server server;

    private final AggregatorConfiguration config;

    private final AggregatorInfo info;

    private final String url;

    private List<String> torrentTrackers = new ArrayList<>();



    /**
     * Creates an {@link Aggregator} with default settings.
     * @param database The {@link ZWIDatabase} to host
     */
    public Aggregator(ZWIDatabase database) throws IOException {
        this(new AggregatorConfiguration(database));
    }

    /**
     * Creates an {@link Aggregator}.
     * @param config The {@link AggregatorConfiguration} to use.
     *               Changes made to the configuration after the aggregator is started are not guaranteed to have an effect.
     */
    public Aggregator(AggregatorConfiguration config) throws IOException {
        this.config = config;
        this.database = config.getDatabase();

        String host = config.getHost();
        int port = config.getPort();
        // Add the port to the end of the URL if it's not the default port (80 for HTTP, 443 for HTTPS)
        String portPart = (config.isSSLEnabled() ? (port == 443 ? "" : ":" + port) : (port == 80 ? "" : ":" + port));
        this.url = (config.isSSLEnabled() ? "https://" : "http://") + config.getHost() + portPart;
        database.updateInfo();
        this.info = new AggregatorInfo(config.getName(), config.getDescription(), database.getArticleCount(), database.getSize());

        server = new Server();
        ServerConnector connector;
        if(config.isSSLEnabled()) {
            HttpConfiguration httpsConfig = new HttpConfiguration();
            httpsConfig.setCustomizers(Collections.singletonList(new SecureRequestCustomizer()));

            SslContextFactory.Server sslContextFactory = new SslContextFactory.Server();
            sslContextFactory.setKeyStorePath(config.getKeystoreFile());
            sslContextFactory.setKeyStorePassword(config.getKeystorePassword());

            if(config.disableSNICheck()) {
                SecureRequestCustomizer customizer = new SecureRequestCustomizer();
                customizer.setSniHostCheck(false);
                httpsConfig.addCustomizer(customizer);
            }

            connector = new ServerConnector(server, new SslConnectionFactory(sslContextFactory, "http/1.1"), new HttpConnectionFactory(httpsConfig));
        } else {
            connector = new ServerConnector(server);
        }
        connector.setHost(host);
        connector.setPort(port);
        server.addConnector(connector);

        ServletContextHandler servletHandler = new ServletContextHandler();

        // Set up static files
        ServletHolder resourceHolder = new ServletHolder("default", DefaultServlet.class);
        resourceHolder.setInitParameter("resourceBase", database.getRoot().toAbsolutePath().toString());
        resourceHolder.setInitParameter("dirAllowed", "true");
        resourceHolder.setInitParameter("pathInfoOnly", "true");
        URL stylesheetURL = getClass().getResource("/aggregator/style.css");
        if(stylesheetURL == null) ExceptionHandler.getExceptionHandler().accept(new IOException("Failed to read /aggregator/style.css"));
        else resourceHolder.setInitParameter("stylesheet", stylesheetURL.toExternalForm());
        servletHandler.addServlet(resourceHolder, "/db/*");

        // Set up the REST API
        servletHandler.addServlet(WelcomeServlet.class, "/");
        servletHandler.addServlet(InfoServlet.class, "/info");
        servletHandler.addServlet(AggregatorListServlet.class, "/aggregators");
        servletHandler.addServlet(IndexListServlet.class, "/indexes");
        servletHandler.addServlet(SearchServlet.class, "/search");
        //servletHandler.addServlet(DatabaseRootServlet.class, "/db"); TODO
        servletHandler.addServlet(ZWIDownloadServlet.class, "/download/zwi");
        servletHandler.addServlet(TorrentDownloadServlet.class, "/download/torrent");
        servletHandler.addServlet(ZWIMetadataServlet.class, "/download/metadata");
        servletHandler.addServlet(GetURLServlet.class, "/download/url");
        ServletHolder uploadHolder = servletHandler.addServlet(UploadServlet.class, "/upload");
        MultipartConfigElement multipartConfig = new MultipartConfigElement(
                config.getDatabase().getRoot().getParent().resolve("temp").toAbsolutePath().toString(),
                config.getMaxUploadSize(),
                config.getMaxUploadSize(),
                (int) config.getFileSizeThreshold());
        uploadHolder.getRegistration().setMultipartConfig(multipartConfig);

        // Add a rate-limiting filter
        servletHandler.addFilter(RateLimitFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));

        server.setHandler(servletHandler);

        Aggregators.registerLocalAggregator(this);

        // Update the AggregatorInfo (article count and database size) every 30 minutes
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        scheduler.scheduleAtFixedRate(() -> {
            try {
                database.updateInfo();
            } catch(IOException e) {
                logException("Error updating database info", e);
            }
            info.setArticleCount(database.getArticleCount());
            info.setDatabaseSize(database.getSize());
        }, 0, 30, TimeUnit.MINUTES);


        // If enabled, update the list of trackers using newTrackon's API every 12 hours
        if(config.addTrackersToTorrents()) {
            scheduler.scheduleAtFixedRate(() -> {
                try {
                    log("Getting list of trackers from newTrackon...");
                    HttpResponse response = makeRequest(new HttpGet("https://newtrackon.com/api/stable?include_ipv6_only_trackers=false"), 10000);
                    if(response.getStatusLine().getStatusCode() == 200) {
                        byte[] trackerListBytes = response.getEntity().getContent().readAllBytes();
                        torrentTrackers = Arrays.stream(new String(trackerListBytes).split("\n")).filter(str -> !str.isEmpty()).toList();
                        log("Success. Got " + torrentTrackers.size() + " trackers");
                    }
                } catch(IOException e) {
                    logException("Error getting list of trackers from newTrackon", e);
                }
            }, 0, 12, TimeUnit.HOURS);
        }
    }


    /**
     * @hidden
     */
    public static class RateLimitFilter implements Filter {

        private static final HashMap<String, Long> requestsInLastSecond = new HashMap<>(),
                                                      requestsInLastDay = new HashMap<>();

        // Clear the HashMaps when needed.
        // One is cleared every second, while the other is cleared every day.
        static {
            ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
            scheduler.scheduleAtFixedRate(requestsInLastSecond::clear, 1, 1, TimeUnit.SECONDS);
            scheduler.scheduleAtFixedRate(requestsInLastDay::clear, 1, 1, TimeUnit.DAYS);
        }

        @Override
        public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
            String ip = request.getRemoteAddr();
            Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
            assert aggregator != null;

            if(!requestsInLastSecond.containsKey(ip)) requestsInLastSecond.put(ip, 1L);
            else requestsInLastSecond.put(ip, requestsInLastSecond.get(ip) + 1);

            if(!requestsInLastDay.containsKey(ip)) requestsInLastDay.put(ip, 1L);
            else requestsInLastDay.put(ip, requestsInLastDay.get(ip) + 1);

            long perSecondLimit = aggregator.getConfig().getMaxRequestsPerSecond();
            if(perSecondLimit != -1 && requestsInLastSecond.get(ip) - 1 >= perSecondLimit) {
                sendError((HttpServletResponse) response, 429, "Too many requests. Limit is " + perSecondLimit + "/second");
                return;
            }

            long perDayLimit = aggregator.getConfig().getMaxRequestsPerDay();
            if(perDayLimit != -1 && requestsInLastDay.get(ip) - 1 >= perDayLimit) {
                sendError((HttpServletResponse) response, 429, "Too many requests. Limit is " + perDayLimit + "/day");
                return;
            }

            chain.doFilter(request, response);
        }

    }



    /**
     * Starts this {@link Aggregator}'s internal server.
     */
    public void start() throws Exception {
        server.start();
    }

    /**
     * Stops this {@link Aggregator}'s internal server.
     */
    public void stop() throws Exception {
        server.stop();
    }

    /**
     * @return This {@link Aggregator}'s internal server
     */
    public Server getServer() {
        return server;
    }

    /**
     * @return An {@link AggregatorInfo} object, containing information about this {@link Aggregator}
     */
    public AggregatorInfo getInfo() {
        return info;
    }

    /**
     * @return This {@link Aggregator}'s configuration.
     *         Note that changing the properties after the {@code Aggregator} has been created
     *         won't do anything.
     */
    public AggregatorConfiguration getConfig() {
        return config;
    }

    /**
     * @return A {@link List<String>} of trackers added to torrents.
     *           If {@link AggregatorConfiguration#addTrackersToTorrents()} is {@code true},
     *           this method will return a list of trackers from <a href="https://newtrackon.com/list">newTrackon</a>, updated every 12 hours.
     *           Otherwise, it will return an empty list.
     */
    public List<String> getTorrentTrackers() {
        return torrentTrackers;
    }

    /**
     * @return The {@link ZWIDatabase} this {@link Aggregator} is hosting
     */
    public ZWIDatabase getDatabase() {
        return database;
    }

    /**
     * @return This {@link Aggregator}'s URL
     */
    public String getURL() {
        return url;
    }

    /**
     * Deregisters this {@link Aggregator}, and destroys its internal server.
     */
    public void destroy() throws Exception {
        Aggregators.deregisterLocalAggregator(this);
        server.stop();
        server.destroy();
    }

}
