package org.encyclosphere.encyclotorrent.api.aggregator;

import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Contains {@link Aggregator} settings - things like the name, host, port, whether to enable HTTPS, etc.
 * These settings can be changed after the aggregator is started, but the change may not have an effect.
 */
public class AggregatorConfiguration {

    private ZWIDatabase database;

    private String host = "0.0.0.0";
    private int port = 22308;

    private boolean enableSSL = false;
    private String keystoreFile, keystorePassword;

    private String name, description;

    private boolean addTrackersToTorrents = true, disableSNICheck = false;

    private final ArrayList<String> publisherBlacklist = new ArrayList<>();
    private final ArrayList<String> publisherWhitelist = new ArrayList<>();
    private ListType selectedList = ListType.WHITELIST;

    private long maxRequestsPerSecond, maxRequestsPerDay, maxUploadSize, fileSizeThreshold;

    private final HashMap<String, String> passwords = new HashMap<>();

    public enum ListType { BLACKLIST, WHITELIST, NONE }



    private AggregatorConfiguration() {}

    /**
     * Creates a default {@link AggregatorConfiguration}.
     * @param database The {@link ZWIDatabase} to host
     */
    public AggregatorConfiguration(ZWIDatabase database) {
        this.database = database;
    }


    /**
     * Creates an aggregator configuration file.
     * @param configPath The {@link Path} to the file
     * @throws IOException If the file couldn't be created (e.g. if it already exists)
     */
    public static void createConfigFile(Path configPath) throws IOException {
        InputStream defaultConfigStream = AggregatorConfiguration.class.getResourceAsStream("/aggregator/aggregator.json");
        if(defaultConfigStream == null) throw new IOException("Failed to read /aggregator/aggregator.json");
        String defaultConfig = new String(defaultConfigStream.readAllBytes());
        Files.writeString(configPath, defaultConfig, StandardOpenOption.CREATE_NEW);
    }

    /**
     * Reads an {@link AggregatorConfiguration} from a file.
     * @param aggregatorDir The root directory of an aggregator, containing an {@code aggregator.json} file
     * @return The {@link AggregatorConfiguration}, read from {@code aggregator.json}
     * @throws IOException If the file couldn't be read
     * @throws InvalidConfigurationException If the file isn't a valid aggregator configuration file
     */
    public static AggregatorConfiguration load(Path aggregatorDir) throws IOException, InvalidConfigurationException {
        try {
            return load(new JSONObject(Files.readString(aggregatorDir.resolve("aggregator.json"))), new ZWIDatabase(aggregatorDir.resolve("db")));
        } catch(JSONException e) {
            throw new InvalidConfigurationException(e);
        }
    }

    /**
     * Creates an {@link AggregatorConfiguration} from a {@link JSONObject}.
     * @param object A {@link JSONObject} with configuration information
     * @param database The {@link ZWIDatabase} to use
     * @return The resulting {@link AggregatorConfiguration}
     * @throws InvalidConfigurationException If a required field is missing or invalid
     */
    public static AggregatorConfiguration load(JSONObject object, ZWIDatabase database) throws InvalidConfigurationException {
        try {
            AggregatorConfiguration config = new AggregatorConfiguration();

            config.setDatabase(database);

            config.setHost(object.getString("Host"));
            config.setPort(object.getInt("Port"));

            config.setSSLEnabled(object.getBoolean("EnableSSL"));
            config.setKeystoreFile(object.getString("KeystoreFile"));
            config.setKeystorePassword(object.getString("KeystorePassword"));

            config.setName(object.getString("Name"));
            config.setDescription(object.getString("Description"));

            if(object.has("AddTrackersToTorrents"))
                config.setAddTrackersToTorrents(object.getBoolean("AddTrackersToTorrents"));

            if(object.has("DisableSNICheck"))
                config.setDisableSNICheck(object.getBoolean("DisableSNICheck"));

            JSONArray publisherBlacklistArray = object.getJSONArray("PublisherBlacklist");
            for(int i = 0; i < publisherBlacklistArray.length(); i++) {
                config.getPublisherBlacklist().add(publisherBlacklistArray.getString(i));
            }

            JSONArray publisherWhitelistArray = object.getJSONArray("PublisherWhitelist");
            for(int i = 0; i < publisherWhitelistArray.length(); i++) {
                config.getPublisherWhitelist().add(publisherWhitelistArray.getString(i));
            }

            if(object.getString("SelectedList").equals("blacklist")) config.setSelectedList(ListType.BLACKLIST);
            else if(object.getString("SelectedList").equals("whitelist")) config.setSelectedList(ListType.WHITELIST);
            else if(object.getString("SelectedList").equals("none")) config.setSelectedList(ListType.NONE);
            else throw new InvalidConfigurationException("SelectedList must be \"blacklist\", \"whitelist\", or \"none\"");

            config.setMaxRequestsPerSecond(object.getLong("MaxRequestsPerSecond"));
            config.setMaxRequestsPerDay(object.getLong("MaxRequestsPerDay"));
            config.setMaxUploadSize(object.getLong("MaxUploadSize"));
            config.setFileSizeThreshold(object.getLong("FileSizeThreshold"));

            JSONArray passwordsArray = object.getJSONArray("Passwords");
            for(int i = 0; i < passwordsArray.length(); i++) {
                JSONObject password = passwordsArray.getJSONObject(i);
                config.addPassword(password.getString("Publisher"), password.getString("Password"));
            }

            return config;
        } catch(JSONException e) {
            throw new InvalidConfigurationException(e);
        }
    }

    /**
     * Indicates that the JSON being used to create an aggregator configuration file is invalid.
     */
    public static class InvalidConfigurationException extends Exception {

        InvalidConfigurationException(String message) {
            super(message);
        }

        InvalidConfigurationException(Throwable cause) {
            super(cause);
        }

    }


    /**
     * @return The {@link ZWIDatabase} to host
     */
    public ZWIDatabase getDatabase() {
        return database;
    }

    /**
     * Sets the {@link ZWIDatabase} to host.
     * @param database A {@code ZWIDatabase}
     */
    public void setDatabase(ZWIDatabase database) {
        this.database = database;
    }

    /**
     * @return The IP address/domain name to listen on
     */
    public String getHost() {
        return host;
    }

    /**
     * Sets the IP address/domain name to listen on.
     * @param host An IP address or domain name
     */
    public void setHost(String host) {
        this.host = host;
    }

    /**
     * @return The port to listen on
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the port to listen on.
     * @param port The port
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * @return Whether HTTPS/SSL is enabled
     */
    public boolean isSSLEnabled() {
        return enableSSL;
    }

    /**
     * Sets whether HTTPS/SSL is enabled.
     * @param enableSSL Whether to enable HTTPS/SSL
     */
    public void setSSLEnabled(boolean enableSSL) {
        this.enableSSL = enableSSL;
    }

    /**
     * @return The path to the keystore file to use for HTTPS/SSL.
     */
    public String getKeystoreFile() {
        return keystoreFile;
    }

    /**
     * Sets the path to the keystore file to use for HTTPS/SSL.
     * @param keystoreFile The path to a keystore file
     * @see AggregatorConfiguration#setKeystorePassword(String)
     */
    public void setKeystoreFile(String keystoreFile) {
        this.keystoreFile = keystoreFile;
    }

    /**
     * @return The password for the keystore file
     */
    public String getKeystorePassword() {
        return keystorePassword;
    }

    /**
     * Sets the password for the keystore file.
     * @param keystorePassword The password
     * @see AggregatorConfiguration#setKeystoreFile(String)
     */
    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    /**
     * @return The aggregator's name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the aggregator's name.
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Some information about the aggregator
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the aggregator's description.
     * @param description Some information about the aggregator
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return Whether trackers should be added to torrents
     */
    public boolean addTrackersToTorrents() {
        return addTrackersToTorrents;
    }

    /**
     * Sets whether to add trackers to torrents.
     * @param addTrackersToTorrents Whether to add trackers to torrents
     */
    public void setAddTrackersToTorrents(boolean addTrackersToTorrents) {
        this.addTrackersToTorrents = addTrackersToTorrents;
    }

    /**
     * @return Whether the SNI check should be disabled.
     */
    public boolean disableSNICheck() {
        return disableSNICheck;
    }

    /**
     * Sets whether to disable the SNI check.
     * @param disableSNICheck Whether to disable the SNI check
     */
    public void setDisableSNICheck(boolean disableSNICheck) {
        this.disableSNICheck = disableSNICheck;
    }

    /**
     * @return An {@link ArrayList<String>} of publishers to disallow uploads from.
     *         If this list is selected, uploading articles from publishers on the list will be disallowed.
     */
    public ArrayList<String> getPublisherBlacklist() {
        return publisherBlacklist;
    }

    /**
     * @return An {@link ArrayList<String>} of publishers to allow uploads from.
     *         If this list is selected, uploading articles from publishers not on the list will be disallowed.
     */
    public ArrayList<String> getPublisherWhitelist() {
        return publisherWhitelist;
    }

    /**
     * @return The list to use to restrict article uploads
     */
    public ListType getSelectedList() {
        return selectedList;
    }

    /**
     * Sets the list to use to restrict article uploads.
     * @param selectedList The {@link ListType} to use
     * @see AggregatorConfiguration#getPublisherBlacklist()
     * @see AggregatorConfiguration#getPublisherWhitelist()
     */
    public void setSelectedList(ListType selectedList) {
        this.selectedList = selectedList;
    }

    /**
     * @return The maximum number of requests allowed per second from each IP address.
     *         If this is -1, the number of requests per second is unlimited.
     */
    public long getMaxRequestsPerSecond() {
        return maxRequestsPerSecond;
    }

    /**
     * Sets the maximum number of requests allowed per second from each IP address.
     * If set to -1, the number of requests per second will be unlimited.
     * @param maxRequestsPerSecond The maximum number of requests
     */
    public void setMaxRequestsPerSecond(long maxRequestsPerSecond) {
        this.maxRequestsPerSecond = maxRequestsPerSecond;
    }

    /**
     * @return The maximum number of requests allowed per day from each IP address.
     *         If this is -1, the number of requests per day is unlimited.
     */
    public long getMaxRequestsPerDay() {
        return maxRequestsPerDay;
    }

    /**
     * Sets the maximum number of requests allowed per day from each IP address.
     * If set to -1, the number of requests per day will be unlimited.
     * @param maxRequestsPerDay The maximum number of requests
     */
    public void setMaxRequestsPerDay(long maxRequestsPerDay) {
        this.maxRequestsPerDay = maxRequestsPerDay;
    }

    /**
     * @return The maximum size of uploaded articles, in bytes
     */
    public long getMaxUploadSize() {
        return maxUploadSize;
    }

    /**
     * Sets the maximum size of uploaded articles.
     * @param maxUploadSize The maximum size, in bytes
     */
    public void setMaxUploadSize(long maxUploadSize) {
        this.maxUploadSize = maxUploadSize;
    }

    /**
     * @return The file size threshold, in bytes. If an uploaded file is larger than this threshold, it's written to disk.
     */
    public long getFileSizeThreshold() {
        return fileSizeThreshold;
    }

    /**
     * Sets the file size threshold. If an uploaded file is larger than this threshold, it's written to disk.
     * @param fileSizeThreshold The threshold, in bytes
     */
    public void setFileSizeThreshold(long fileSizeThreshold) {
        this.fileSizeThreshold = fileSizeThreshold;
    }

    /**
     * @return A {@link HashMap} of publishers and upload passwords.
     */
    public HashMap<String, String> getPasswords() {
        return passwords;
    }

    /**
     * Adds an upload password.
     * @param publisher A publisher
     * @param password The password required to upload articles from that publisher
     */
    public void addPassword(String publisher, String password) {
        passwords.put(publisher, password);
    }

}
