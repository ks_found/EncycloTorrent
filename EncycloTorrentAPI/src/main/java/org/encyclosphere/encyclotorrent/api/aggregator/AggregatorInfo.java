package org.encyclosphere.encyclotorrent.api.aggregator;

import org.json.JSONObject;

/**
 * Contains information about an {@link Aggregator}.
 */
public class AggregatorInfo {

    private final String apiVersion;

    private final String name, description;

    private long articleCount, databaseSize;



    AggregatorInfo(String name, String description, long articleCount, long databaseSize) {
        this.apiVersion = Aggregator.API_VERSION;
        this.name = name;
        this.description = description;
        this.articleCount = articleCount;
        this.databaseSize = databaseSize;
    }

    AggregatorInfo(JSONObject object) {
        this.apiVersion = object.getString("APIversion");
        this.name = object.getString("Name");
        this.description = object.getString("Description");
        this.articleCount = object.getLong("ArticleCount");
        this.databaseSize = object.getLong("DatabaseSize");
    }


    /**
     * @return The aggregator's API version (currently 1.0)
     */
    public String getAPIVersion() {
        return apiVersion;
    }

    /**
     * @return The aggregator's name
     */
    public String getName() {
        return name;
    }

    /**
     * @return Some information about the aggregator
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return The number of articles in the aggregator's database
     */
    public long getArticleCount() {
        return articleCount;
    }

    void setArticleCount(long articleCount) {
        this.articleCount = articleCount;
    }

    /**
     * @return The size of the aggregator's database, in bytes
     */
    public long getDatabaseSize() {
        return databaseSize;
    }

    /**
     * Sets the size of the aggregator's database.
     * @param databaseSize The size of the database, in bytes
     */
    void setDatabaseSize(long databaseSize) {
        this.databaseSize = databaseSize;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("APIversion", Aggregator.API_VERSION);
        object.put("Name", name);
        object.put("Description", description);
        object.put("ArticleCount", articleCount);
        object.put("DatabaseSize", databaseSize);
        return object;
    }

}
