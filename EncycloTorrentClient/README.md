# EncycloTorrentClient

A BitTorrent client for the [Encyclosphere](https://encyclosphere.org).

## Installation
For installation instructions, refer to [this wiki page](https://gitlab.com/ks_found/EncycloTorrent/-/wikis/Installation).
