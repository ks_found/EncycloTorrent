package org.encyclosphere.encyclotorrent.api.misc;

import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.libtorrent4j.*;
import org.libtorrent4j.alerts.Alert;
import org.libtorrent4j.alerts.AlertType;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.*;
import java.util.concurrent.CountDownLatch;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.log;

/**
 * Contains methods for creating and downloading torrents.
 * The methods in this class are not intended to be used directly, except for {@link TorrentClient#init()} and {@link TorrentClient#init(Path)}.
 */
public class TorrentClient {

    private static SessionManager sessionManager = null;



    /**
     * Initializes libtorrent4j, copying native libraries to ~/.encyclotorrent.
     * This method must be called before using any other {@link TorrentClient} methods.
     */
    public static void init() throws IOException {
        init(Path.of(System.getProperty("user.home"), ".encyclotorrent"));
    }

    /**
     * Initializes libtorrent4j, copying native libraries to the specified directory.
     * This method must be called before using any other {@link TorrentClient} methods.
     * @param nativesPath The directory to put native libraries in, as a {@link Path}
     */
    public static void init(Path nativesPath) throws IOException {
        Files.createDirectories(nativesPath);
        String osName = System.getProperty("os.name");
        boolean isLinux = osName.toLowerCase().contains("linux"),
                isMac = osName.toLowerCase().contains("mac"),
                isWindows = osName.toLowerCase().contains("windows");
        String filename = null;
        if(isLinux) filename = "libtorrent4j.so";
        else if(isMac) filename = "libtorrent4j.dylib";
        else if(isWindows) filename = "libtorrent4j.dll";
        assert filename != null;
        Path nativePath = nativesPath.resolve(filename);
        // TODO Make this get the file from the libtorrent4j native JAR
        if(Files.notExists(nativePath)) {
            InputStream nativeStream = Utilities.class.getClassLoader().getResourceAsStream("natives/" + filename);
            if(nativeStream == null) throw new IOException("Failed to read natives/" + filename);
            byte[] nativeBytes = nativeStream.readAllBytes();
            Files.write(nativePath, nativeBytes);
        }
        System.setProperty("libtorrent4j.jni.path", nativePath.toAbsolutePath().toString());
        sessionManager = new SessionManager();
    }

    /**
     * Creates a torrent.
     * @param toShare The {@link ZWIFile} to share
     * @param trackers A {@link List<String>} of trackers to add to the torrent
     * @param webseeds An {@link List<String>} of webseeds to add to the torrent
     * @return The resulting torrent, as a byte array
     */
    public static byte[] createTorrent(ZWIFile toShare, List<String> trackers, List<String> webseeds) throws IOException {
        // libtorrent4j has methods for creating torrents,
        // but it doesn't support changing the filename, so I used some custom code (see below)
        String filename = toShare.getMetadata().getTitle() + ".zwi";
        return createTorrent(toShare.getLocation(), filename, toShare.getID(), trackers, webseeds);
    }

    /**
     * Downloads a file using BitTorrent.
     * @param info The {@link TorrentInfo}
     * @param downloadDir The {@link Path} to the directory to put the downloaded file in
     * @throws InterruptedException If the download thread is interrupted
     */
    public static void downloadFile(TorrentInfo info, Path downloadDir) throws InterruptedException {
        if(sessionManager == null) throw new IllegalStateException("TorrentClient hasn't been initialized. You must call TorrentClient#init() before using any TorrentClient methods.");
        String filename = info.files().fileName(0);
        log("Downloading " + filename + "...");
        CountDownLatch downloadLatch = new CountDownLatch(1);
        AlertListener alertListener = new AlertListener() {
            @Override
            public int[] types() {
                return null;
            }

            @Override
            public void alert(Alert<?> alert) {
                log(alert.message());
                if(alert.type() == AlertType.TORRENT_FINISHED && alert.message().split(" torrent finished downloading")[0].equals(filename)) {
                    downloadLatch.countDown();
                }
            }
        };
        sessionManager.addListener(alertListener);
        sessionManager.start();
        sessionManager.download(info, downloadDir.toFile());
        downloadLatch.await();
        sessionManager.removeListener(alertListener);
    }

    /**
     * Downloads a file using BitTorrent.
     * @param torrentBytes The torrent bytes
     * @param downloadDir The {@link Path} to put the downloaded file in
     */
    public static void downloadFile(byte[] torrentBytes, Path downloadDir) throws InterruptedException {
        // TODO Add all webseeds
        downloadFile(new TorrentInfo(torrentBytes), downloadDir);
    }

    /**
     * Downloads a file using BitTorrent.
     * @param torrent The {@link Path} to the torrent
     * @param downloadDir The {@link Path} to put the downloaded file in
     */
    public static void downloadFile(Path torrent, Path downloadDir) throws IOException, InterruptedException {
       downloadFile(Files.readAllBytes(torrent), downloadDir);
    }

    /**
     * Stops downloading/seeding a torrent.
     * @param infoHash The infohash of the torrent
     */
    public static void stopDownloading(Sha1Hash infoHash) {
        sessionManager.remove(sessionManager.find(infoHash));
    }

    /**
     * Stops downloading/seeding a torrent.
     * @param info The {@link TorrentInfo} used to identify the torrent
     */
    public static void stopDownloading(TorrentInfo info) {
        stopDownloading(info.infoHash());
    }

    // Code from https://stackoverflow.com/a/2033298/5905216

    private static byte[] createTorrent(Path torrentPath, String filename, String id, List<String> trackers, List<String> webseeds) throws IOException {
        final int pieceLength = 524288;
        Map<String, Object> info = new HashMap<>();
        info.put("name", filename);
        info.put("length", Files.size(torrentPath));
        info.put("piece length", pieceLength);
        info.put("pieces", hashPieces(torrentPath.toFile(), pieceLength));
        Map<String, Object> metaInfo = new HashMap<>();
        if(trackers != null && !trackers.isEmpty()) {
            metaInfo.put("announce", trackers.get(0));
            if(trackers.size() > 1) metaInfo.put("announce-list", new Object[] { trackers.toArray() });
        }
        metaInfo.put("creation date", Instant.now().getEpochSecond());
        metaInfo.put("article ID", id);
        metaInfo.put("info", info);
        if(webseeds != null && !webseeds.isEmpty()) metaInfo.put("url-list", webseeds.toArray());
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        encodeMap(metaInfo, out);
        out.close();
        return out.toByteArray();
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    private static void encodeObject(Object o, OutputStream out) throws IOException {
        if(o instanceof String)
            encodeString((String) o, out);
        else if(o instanceof Map)
            encodeMap((Map) o, out);
        else if(o instanceof byte[])
            encodeBytes((byte[]) o, out);
        else if(o instanceof Number)
            encodeLong(((Number) o).longValue(), out);
        else if(o instanceof String[])
            encodeStringList((String[]) o, out);
        else if(o instanceof Object[])
            encodeObjectList((Object[]) o, out);
        else
            throw new IllegalArgumentException("Unencodable type: " + o);
    }

    private static void encodeLong(long value, OutputStream out) throws IOException {
        out.write('i');
        out.write(Long.toString(value).getBytes(StandardCharsets.US_ASCII));
        out.write('e');
    }

    private static void encodeBytes(byte[] bytes, OutputStream out) throws IOException {
        out.write(Integer.toString(bytes.length).getBytes(StandardCharsets.US_ASCII));
        out.write(':');
        out.write(bytes);
    }

    private static void encodeString(String str, OutputStream out) throws IOException {
        encodeBytes(str.getBytes(StandardCharsets.UTF_8), out);
    }

    private static void encodeMap(Map<String, Object> map, OutputStream out) throws IOException {
        // Sort the map. A generic encoder should sort by key bytes
        SortedMap<String, Object> sortedMap = new TreeMap<>(map);
        out.write('d');
        for(Map.Entry<String, Object> e : sortedMap.entrySet()) {
            encodeString(e.getKey(), out);
            encodeObject(e.getValue(), out);
        }
        out.write('e');
    }

    private static void encodeStringList(String[] values, OutputStream out) throws IOException {
        out.write('l');
        for(String value : values) encodeString(value, out);
        out.write('e');
    }

    private static void encodeObjectList(Object[] values, OutputStream out) throws IOException {
        out.write('l');
        for(Object value : values) encodeObject(value, out);
        out.write('e');
    }

    static MessageDigest sha1 = null;
    static {
        try {
            sha1 = MessageDigest.getInstance("SHA-1");
        } catch(NoSuchAlgorithmException e) {
            ExceptionHandler.getExceptionHandler().accept(e);
        }
    }

    @SuppressWarnings("SameParameterValue")
    private static byte[] hashPieces(File file, int pieceLength) throws IOException {
        sha1.reset();
        InputStream in = new FileInputStream(file);
        ByteArrayOutputStream pieces = new ByteArrayOutputStream();
        byte[] bytes = new byte[pieceLength];
        int pieceByteCount = 0, readCount = in.read(bytes, 0, pieceLength);
        while(readCount != -1) {
            pieceByteCount += readCount;
            sha1.update(bytes, 0, readCount);
            if (pieceByteCount == pieceLength) {
                pieceByteCount = 0;
                pieces.write(sha1.digest());
            }
            readCount = in.read(bytes, 0, pieceLength - pieceByteCount);
        }
        in.close();
        if(pieceByteCount > 0)
            pieces.write(sha1.digest());
        return pieces.toByteArray();
    }

}
