package org.encyclosphere.encyclotorrent.api.index;

import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * Contains information about multiple ZWI files.
 */
public class IndexFile {

    public static final String HEADER = "id,url,title,filesize,timestamp\n";

    private Path location = null;

    private final ArrayList<String> entries;

    /**
     * Creates an empty {@link IndexFile} object.
     */
    public IndexFile() {
        entries = new ArrayList<>();
    }

    /**
     * Creates an {@link IndexFile} object.
     * @param location The {@link Path} to the index file
     */
    public IndexFile(Path location) throws IOException {
        this.location = location;

        if(Files.exists(location)) {
            String[] entriesArray = new String(Utilities.readGzippedBytes(location)).split("\n");
            entries = new ArrayList<>(Arrays.asList(entriesArray));
            entries.remove(0); // Remove the header
        } else {
            Files.createDirectories(location.getParent());
            Files.createFile(location);
            entries = new ArrayList<>();
        }
    }

    /**
     * Creates an {@link IndexFile} object from a {@link String}.
     * @param source The source {@code String}
     */
    public IndexFile(String source) {
        String[] entriesArray = source.split("\n");
        entries = new ArrayList<>(Arrays.asList(entriesArray));
        entries.remove(0); // Remove the header
    }

    /**
     * @return This {@link IndexFile}'s location on disk
     */
    public Path getLocation() {
        return location;
    }

    public void setLocation(Path location) {
        this.location = location;
    }

    /**
     * @return A {@link List<String>} of this {@link IndexFile}'s entries.<br>
     *         This method should be less expensive than {@link IndexFile#getEntries()}.<br>
     *         You can parse the entries using {@link IndexEntry#IndexEntry(String)}, or use {@link IndexFile#getEntries()}.
     */
    public List<String> getEntryStrings() {
        return entries;
    }

    /**
     * @return A {@link List<IndexEntry>} of this {@link IndexFile}'s entries
     */
    public List<IndexEntry> getEntries() {
        return entries.stream().map(IndexEntry::new).toList();
    }

    /**
     * Adds an entry for the given {@link ZWIFile} to this {@link IndexFile}
     * @param file The file to create an entry for
     * @param removeOldEntries Whether to remove other entries with the same ID
     * @throws UnsupportedOperationException If the given {@code ZWIFile} isn't in a database
     */
    public void addEntry(ZWIFile file, boolean removeOldEntries) throws IOException {
        if(!file.isInDatabase()) throw new UnsupportedOperationException("File isn't in a database");

        if(removeOldEntries) removeEntries(file);

        ZWIMetadata metadata = file.getMetadata();
        Path path = file.getLocation();

        StringBuilder indexLineBuilder = new StringBuilder();
        indexLineBuilder
                .append(metadata.getID()).append(",")                                    // id
                .append("\"").append(metadata.getSourceURL()).append("\"").append(",")   // url
                .append("\"").append(metadata.getTitle()).append("\"").append(",")       // title
                .append(Files.size(path)).append(",")                                    // filesize
                .append(metadata.getLastModifiedEpochSeconds());                         // timestamp

        entries.add(indexLineBuilder.toString());
    }

    /**
     * @param file A {@link ZWIFile}
     * @return This {@link IndexFile}'s {@link IndexEntry} for the given file, or {@code null} if it doesn't exist
     */
    public IndexEntry getEntry(ZWIFile file) throws IOException {
        for(String entry : entries) if(new IndexEntry(entry).getID().equals(file.getID())) return new IndexEntry(entry);
        return null;
    }

    /**
     * @param id An ID
     * @return This {@link IndexFile}'s {@link IndexEntry} with the given ID, or {@code null} if it doesn't exist
     */
    public IndexEntry getEntry(String id) {
        for(String entry : entries) if(new IndexEntry(entry).getID().equals(id)) return new IndexEntry(entry);
        return null;
    }

    /**
     * Removes all entries for the given {@link ZWIFile} from this {@link IndexFile}, if present.
     * @param file The file to remove from the index
     */
    public void removeEntries(ZWIFile file) throws IOException {
        Iterator<String> iterator = entries.iterator();
        while(iterator.hasNext()) {
            String entry = iterator.next();
            if(new IndexEntry(entry).getID().equals(file.getID())) iterator.remove();
        }
    }

    /**
     * @param file A {@link ZWIFile}
     * @return Whether this {@link IndexFile} has an entry with the same ID as the given {@code ZWIFile}
     */
    public boolean hasEntry(ZWIFile file) throws IOException {
        // Don't use getEntry(file) == null to avoid unnecessary instantiation of IndexEntry object
        for(String entry : entries) {
            if(new IndexEntry(entry).getID().equals(file.getID())) return true;
        }
        return false;
    }

    /**
     * @param id An article ID
     * @return Whether this {@link IndexFile} has an entry with the given ID
     */
    public boolean hasEntry(String id) {
        // Don't use getEntry(file) == null to avoid unnecessary instantiation of IndexEntry object
        for(String entry : entries) {
            if(new IndexEntry(entry).getID().equals(id)) return true;
        }
        return false;
    }

    /**
     * @param entry An {@link IndexEntry}
     * @return Whether this {@link IndexFile} has an entry with the same ID as the given {@code IndexEntry}
     */
    public boolean hasEntry(IndexEntry entry) {
        for(String entryStr : entries) {
            IndexEntry entry2 = new IndexEntry(entryStr);
            if(entry.getID().equals(entry2.getID())) return true;
        }
        return false;
    }

    /**
     * Writes the information in this {@link IndexFile} object to the actual index file on disk.
     */
    public void write() throws IOException {
        if(location == null) throw new UnsupportedOperationException("This index file doesn't have a location");
        Utilities.writeGzippedBytes(location, (HEADER + String.join("\n", entries)).getBytes());
    }

}
