package org.encyclosphere.encyclotorrent.api.aggregator.servlets.info;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;

import java.io.IOException;

public class AggregatorListServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        for(String aggregator : Aggregators.getAggregators()) response.getWriter().println(aggregator);
    }

}
