package org.encyclosphere.encyclotorrent.api.aggregator.servlets.info;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.listFiles;

public class IndexListServlet extends HttpServlet {

    // TODO Replace this with /publishers: a list of publishers, not indexes

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;
        ZWIDatabase database = aggregator.getDatabase();

        response.setContentType("text/plain");
        for(Path path : database.getLanguages()) {
            for(Path publisherPath : listFiles(path.resolve("zwi"))) {
                if(!Files.isDirectory(publisherPath)) continue;
                Path indexLocation = publisherPath.resolve("index.csv.gz");
                response.getWriter().println("db/" + database.getRoot().relativize(indexLocation));
            }
        }
    }

}
