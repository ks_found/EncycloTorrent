package org.encyclosphere.encyclotorrent.api.zwi;

import org.encyclosphere.encyclotorrent.api.misc.Utilities;

/**
 * Represents a file within a ZWI file.
 */
public class ZWIContent {

    private String filename;

    private byte[] content;



    /**
     * @return The filename
     */
    public String getFilename() {
        return filename;
    }

    /**
     * Sets the filename.
     * @param filename The filename
     */
    public void setFilename(String filename) {
        this.filename = filename;
    }

    /**
     * @return The contents of the file
     */
    public byte[] getContent() {
        return content;
    }

    /**
     * Sets the contents of the file.
     * @param content The contents, as a byte array
     */
    public void setContent(byte[] content) {
        this.content = content;
    }

    /**
     * Sets the contents of the file.
     * @param content The contents, as a {@link String}
     */
    public void setContent(String content) {
        this.content = content.getBytes();
    }

    /**
     * @return The SHA-1 hash of the contents of the file
     */
    public String getSHA1Hash() {
        return Utilities.getSHA1Hash(content);
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof ZWIContent otherContent) {
            return (otherContent.filename.equals(filename) && otherContent.content == content);
        } else if(obj instanceof String string) {
            return filename.equals(string);
        } else return false;
    }

}
