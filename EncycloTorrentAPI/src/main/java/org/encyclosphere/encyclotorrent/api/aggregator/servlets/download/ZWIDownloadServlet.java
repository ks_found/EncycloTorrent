package org.encyclosphere.encyclotorrent.api.aggregator.servlets.download;

import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;

import java.io.IOException;
import java.nio.file.Files;

public class ZWIDownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String id = request.getParameter("id");
        if(id == null) {
            Utilities.sendError(response, 400, "Missing ID");
            return;
        }

        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        ZWIFile zwiFile = aggregator.getDatabase().getZWIFile(id);
        if(zwiFile == null) {
            Utilities.sendError(response, 404, "Couldn't find an article with the given ID in the database");
            return;
        }

        ZWIMetadata metadata = zwiFile.getMetadata();
        response.setContentType("application/zip");
        response.setHeader("content-disposition", "attachment; filename=\"" + metadata.getTitle() + ".zwi\"");
        response.getOutputStream().write(Files.readAllBytes(zwiFile.getLocation()));
        response.getOutputStream().close();
    }

}
