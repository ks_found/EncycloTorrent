package org.encyclosphere.encyclotorrent.api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIDatabase;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.*;

/**
 * Contains various methods for searching aggregators.
 */
public class Searcher {

    // TODO Add support for excluding publishers

    /**
     * Searches a known aggregator.
     * @param query The text to search for
     * @return A {@link List} of up to 150 search results
     */
    public static ArrayList<ZWIDatabase.SearchResult> search(String query) {
        return search(query, 150);
    }

    /**
     * Searches a known aggregator.
     * @param query The text to search for
     * @param numResults The maximum number of results to return
     * @return A {@link List} of search results
     */
    public static ArrayList<ZWIDatabase.SearchResult> search(String query, int numResults) {
        log("Getting results for \"" + query + "\"...");
        for(String aggregatorURL : Aggregators.getAggregators()) {
            ArrayList<ZWIDatabase.SearchResult> result;
            try {
                result = searchAggregator(aggregatorURL, query, numResults);
            } catch(IOException e) {
                logException("Failed to get results from " + aggregatorURL + " for \"" + query + "\"", e);
                Aggregators.demoteAggregator(aggregatorURL);
                result = null;
            }
            if(result != null) return result;
        }
        return null;
    }

    /**
     * Searches an aggregator.
     * @param query The text to search for
     * @param aggregatorURL The aggregator URL
     * @param numResults The maximum number of results to return
     * @return An {@link ArrayList} of search results
     */
    public static ArrayList<ZWIDatabase.SearchResult> searchAggregator(String aggregatorURL, String query, int numResults) throws IOException {
        String url = aggregatorURL + "/search?q=" + encodeURIComponent(query) + "&numResults=" + numResults;
        log("Getting results for \"" + query + "\" from " + aggregatorURL + "...");

        HttpResponse response = makeRequest(new HttpGet(url));

        if(response.getStatusLine().getStatusCode() == 200) {
            byte[] rawResults = response.getEntity().getContent().readAllBytes();
            JSONArray resultsArray = new JSONObject(new String(rawResults)).getJSONArray("results");
            ArrayList<ZWIDatabase.SearchResult> results = new ArrayList<>();
            for(int i = 0; i < resultsArray.length(); i++) results.add(new ZWIDatabase.SearchResult(resultsArray.getJSONObject(i)));
            log("Success. Got " + results.size() + (results.size() == numResults ? "+" : "") + " results for \"" + query + "\" from " + aggregatorURL);
            return results;
        } else {
            log("Search of " + aggregatorURL + " for \"" + query + "\" failed: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            return null;
        }
    }

}
