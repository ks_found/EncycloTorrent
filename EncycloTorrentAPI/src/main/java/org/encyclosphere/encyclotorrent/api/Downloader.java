package org.encyclosphere.encyclotorrent.api;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Language;
import org.encyclosphere.encyclotorrent.api.misc.TorrentClient;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;
import org.encyclosphere.encyclotorrent.api.zwi.ZWITorrent;
import org.json.JSONObject;
import org.libtorrent4j.TorrentInfo;

import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

import static org.encyclosphere.encyclotorrent.api.misc.Utilities.*;

/**
 * Contains various methods for downloading and getting information about ZWI files from aggregators.
 */
public class Downloader {

    /**
     * Downloads a {@link ZWIFile} from an aggregator.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @param id The ID of the article to download
     * @param targetDirectory The folder to put the article in
     * @return A {@link ZWIFile} reference to the downloaded article, or {@code null} if the article couldn't be downloaded
     * @throws IOException If an error occurs when connecting to the server or writing the file to disk
     * @throws InterruptedException If the download thread is interrupted
     */
    public static ZWIFile downloadZWIFile(String aggregatorURL, String id, Path targetDirectory) throws IOException, InterruptedException {
        String url = aggregatorURL + "/download/torrent?id=" + id;
        log("Downloading a torrent of enc://" + id + " from " + url + "...");

        HttpContext context = new BasicHttpContext();
        HttpGet get = new HttpGet(url);
        get.setConfig(Utilities.requestConfig); // Sets the timeout

        HttpResponse response = httpClient.execute(get, context);
        if(response.getStatusLine().getStatusCode() == 200) {
            byte[] fileContents = response.getEntity().getContent().readAllBytes();
            String filename;
            Path filePath;

            log("Torrent downloaded successfully.");
            log("Downloading ZWI file using torrent...");

            TorrentInfo info = new TorrentInfo(fileContents);
            filename = info.files().fileName(0);
            filePath = targetDirectory.resolve(filename);
            if(Files.exists(filePath)) throw new FileAlreadyExistsException(filePath.toAbsolutePath().toString());
            Files.createDirectories(targetDirectory);
            TorrentClient.downloadFile(info, targetDirectory);

            log("Download complete. Saved as " + filename);
            return new ZWIFile(filePath);
        } else {
            log("Failed to download file from " + url + ": " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            return null;
        }
    }

    /**
     * Downloads a {@link ZWIFile} from the Encyclosphere.
     * @param id The ID of the article to download
     * @param targetDirectory The folder to put the article in
     * @return A {@link ZWIFile} reference to the downloaded article, or {@code null} if the article couldn't be downloaded
     * @throws FileAlreadyExistsException If a file with the same name as the downloaded file already exists
     * @throws InterruptedException If the download thread is interrupted
     */
    public static ZWIFile downloadZWIFile(String id, Path targetDirectory) throws FileAlreadyExistsException, InterruptedException {
        id = processID(id);
        log("Downloading enc://" + id + "...");
        for(String aggregatorURL : Aggregators.getAggregators()) {
            ZWIFile result = null;
            try {
                log("Downloading enc://" + id + " from " + aggregatorURL + "...");
                result = downloadZWIFile(aggregatorURL, id, targetDirectory);
            } catch(FileAlreadyExistsException e) {
                throw e;
            } catch(IOException e) {
                logException("Failed to download file from " + aggregatorURL, e);
            }
            if(result != null) return result;
        }
        return null;
    }

    /**
     * Downloads a {@link ZWIFile} using a torrent file.
     * @param torrent The {@link Path} to the torrent file to use
     * @param targetDirectory The {@link Path} to the directory to put the ZWI file in
     * @return A {@link ZWIFile} reference to the downloaded article, or {@code null} if the article couldn't be downloaded
     * @throws InterruptedException If the download thread is interrupted
     */
    public static ZWIFile downloadZWIFileUsingTorrentFile(Path torrent, Path targetDirectory) throws IOException, InterruptedException {
        byte[] torrentContents = Files.readAllBytes(torrent);
        TorrentInfo info = new TorrentInfo(torrentContents);
        String filename = info.files().fileName(0);
        Path filePath = targetDirectory.resolve(filename);
        TorrentClient.downloadFile(info, targetDirectory);
        return new ZWIFile(filePath);
    }

    /**
     * @param id An article ID
     * @param language The {@link Language} the article is in
     * @param publisher The article's publisher
     * @return An {@link ArrayList<String>} of download links for the article, e.g.:<br>
     *         {@code [http://aggregator1.com:22308/db/en/zwi/example/0/00/&lt;id&gt;.zwi, http://aggregator2.com:22308/db/en/zwi/example/0/00/&lt;id&gt;.zwi]}
     */
    public static ArrayList<String> getDownloadLinks(String id, Language language, String publisher) {
        ArrayList<String> downloadLinks = new ArrayList<>();
        for(String aggregatorURL : Aggregators.getAggregators()) {
            downloadLinks.add(aggregatorURL + "/db/" + language.getCode() + "/zwi/" + publisher + "/" + getPartialPath(id) + "/" + id + ".zwi");
        }
        return downloadLinks;
    }

    /**
     * Downloads a torrent of a ZWI file from an aggregator.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @param id The ID of the article to download
     * @return A {@link ZWITorrent} object with the filename and contents of the torrent
     */
    public static ZWITorrent downloadTorrent(String aggregatorURL, String id) throws IOException {
        id = processID(id);
        String url = aggregatorURL + "/download/torrent?id=" + id;
        log("Downloading torrent from " + url + "...");

        HttpResponse response = makeRequest(new HttpGet(url));

        if(response.getStatusLine().getStatusCode() == 200) {
            String filename = response.getFirstHeader("Content-Disposition").getElements()[0].getParameter(0).getValue();
            byte[] content = response.getEntity().getContent().readAllBytes();
            log("Download complete. Saved as " + filename);
            return new ZWITorrent(filename, content);
        } else {
            log("Failed to download torrent: " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            return null;
        }
    }

    /**
     * Downloads a torrent of a ZWI file from the Encyclosphere.
     * @param id The ID of the article to download
     * @return A {@link ZWITorrent} of the article with the given ID, or {@code null} if the torrent couldn't be downloaded
     */
    public static ZWITorrent downloadTorrent(String id) {
        id = processID(id);
        log("Downloading torrent of enc://" + id + "...");
        for(String aggregatorURL : Aggregators.getAggregators()) {
            ZWITorrent result;
            try {
                log("Downloading torrent of enc://" + id + " from " + aggregatorURL + "...");
                result = downloadTorrent(aggregatorURL, id);
            } catch(IOException e) {
                logException("Failed to download torrent from " + aggregatorURL, e);
                result = null;
            }
            if(result != null) return result;
        }
        return null;
    }

    /**
     * Gets the raw metadata of an article from an aggregator.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @param id The ID of the article to download
     * @return The raw metadata of the article with the given ID, or {@code null} if it couldn't be downloaded
     */
    public static String getRawMetadata(String aggregatorURL, String id) throws IOException {
        id = processID(id);
        String url = aggregatorURL + "/download/metadata?id=" + id;

        log("Getting metadata from " + url + "...");

        HttpResponse response = makeRequest(new HttpGet(url));
        if(response.getStatusLine().getStatusCode() == 200) {
            return new String(response.getEntity().getContent().readAllBytes());
        } else {
            log("Failed to get metadata from " + url + " - " + response.getStatusLine().getStatusCode() + " " + response.getStatusLine().getReasonPhrase());
            return null;
        }
    }

    /**
     * Gets the raw metadata of an article from the Encyclosphere.
     * @param id The ID of the article to download
     * @return The raw metadata of the article with the given ID, or {@code null} if it couldn't be downloaded
     */
    public static String getRawMetadata(String id) {
        id = processID(id);
        log("Getting metadata of enc://" + id + "...");
        for(String aggregatorURL : Aggregators.getAggregators()) {
            String result;
            try {
                log("Getting metadata of enc://" + id + " from " + aggregatorURL + "...");
                result = getRawMetadata(aggregatorURL, id);
            } catch(IOException e) {
                logException("Failed to get metadata from " + aggregatorURL, e);
                result = null;
            }
            if(result != null) return result;
        }
        return null;
    }

    /**
     * Gets the {@link ZWIMetadata} of an article from an aggregator.
     * @param aggregatorURL The URL of the aggregator, without a trailing slash
     * @param id The ID of the article
     * @return The {@link ZWIMetadata} of the article with the given ID, or {@code null} if it couldn't be downloaded
     */
    public static ZWIMetadata getMetadata(String aggregatorURL, String id) throws IOException {
        id = processID(id);
        String rawMetadata = getRawMetadata(aggregatorURL, id);
        if(rawMetadata != null) return new ZWIMetadata(new JSONObject(rawMetadata));
        else return null;
    }

    /**
     * Gets the {@link ZWIMetadata} of an article from the Encyclosphere.
     * @param id The ID of the article
     * @return The {@link ZWIMetadata} of the article with the given ID, or {@code null} if it couldn't be downloaded
     */
    public static ZWIMetadata getMetadata(String id) {
        String rawMetadata = getRawMetadata(id);
        if(rawMetadata != null) return new ZWIMetadata(new JSONObject(rawMetadata));
        else return null;
    }

}
