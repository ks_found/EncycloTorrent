package org.encyclosphere.encyclotorrent.api.aggregator.servlets.upload;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregator;
import org.encyclosphere.encyclotorrent.api.aggregator.AggregatorConfiguration;
import org.encyclosphere.encyclotorrent.api.aggregator.Aggregators;
import org.encyclosphere.encyclotorrent.api.misc.Utilities;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIFile;
import org.encyclosphere.encyclotorrent.api.zwi.ZWIMetadata;

import java.io.IOException;
import java.util.Base64;

public class UploadServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String password = request.getHeader("Authorization");
        if(password != null) {
            // Extract and decode the password (it's encoded using Base64)
            password = password.split("Basic ")[1];
            password = new String(Base64.getDecoder().decode(password.getBytes()));
        }

        Aggregator aggregator = Aggregators.getLocalAggregator(request.getLocalPort());
        assert aggregator != null;

        // Get the uploaded file
        Part uploadedFile = (Part) request.getParts().toArray()[0];
        byte[] fileContents = uploadedFile.getInputStream().readAllBytes();

        // Make sure it's valid
        if(!ZWIFile.isValid(fileContents)) Utilities.sendError(response, 400, "Invalid ZWI file");

        // Get the metadata
        ZWIMetadata metadata = ZWIMetadata.fromZWIBytes(fileContents);
        if(metadata == null) {
            Utilities.sendError(response, 400, "Invalid ZWI file");
            return;
        }

        // Get the publisher; make sure uploading the file is allowed
        String publisher = metadata.getPublisher();
        AggregatorConfiguration config = aggregator.getConfig();
        AggregatorConfiguration.ListType selectedList = config.getSelectedList();
        if(selectedList == AggregatorConfiguration.ListType.BLACKLIST) {
            if(config.getPublisherBlacklist().contains(publisher)) {
                Utilities.sendError(response, 401, "Publisher is on the blacklist");
                return;
            }
        } else if(selectedList == AggregatorConfiguration.ListType.WHITELIST) {
            if(!config.getPublisherWhitelist().contains(publisher)) {
                Utilities.sendError(response, 401, "Publisher isn't on the whitelist");
                return;
            }
        }

        // Check if the password is correct.
        // If the password is correct (or a password isn't required), continue
        String correctPassword = aggregator.getConfig().getPasswords().get(publisher);
        if(correctPassword != null) {
            if(password == null) {
                Utilities.sendError(response, 401, "Password required");
                return;
            } else if(!password.equals(correctPassword)) {
                Utilities.sendError(response, 403, "Invalid password");
                return;
            }
        }

        // Add the file to the database
        aggregator.getDatabase().addFile(metadata, fileContents, true);
    }

}
